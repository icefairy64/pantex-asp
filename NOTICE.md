This software uses the following third-party dependencies:

## FreeImage
This software uses the FreeImage open source image library. See http://freeimage.sourceforge.net for details.

FreeImage is used under the GNU GPL version 3.0 (GPLv3).
## FFmpeg
This software does not link against FFmpeg libraries, but it calls `ffmpeg` and `ffprobe` executables if they are present on the machine running the software.

FFmpeg binaries are not distributed by the software and should be installed by the user manually. 

## React
This software uses <a href=https://github.com/facebook/react>React</a> licensed under the MIT license.

## Create React App
This software uses <a href=https://github.com/facebook/create-react-app>create-react-app</a> licensed under the MIT license.

## MobX
This software uses <a href=https://github.com/mobxjs/mobx>MobX</a> licensed under the MIT license.

## mobx-react
This software uses <a href=https://github.com/mobxjs/mobx-react>mobx-react</a> licensed under the MIT license.

## FontAwesome
This software uses FontAwesome under CC BY 4.0, SIL OFL 1.1 and MIT licenses.

Please see the bundled `LICENSE.txt` file in `src/thirdparty/fontawesome` directory of the frontend module for details.