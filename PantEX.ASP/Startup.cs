﻿using System.Collections.Generic;
using System.IO;
using Breezy.ContentUtil;
using Breezy.PantEX.ASP.FileStorage;
using Breezy.PantEX.ASP.Models;
using Breezy.PantEX.ASP.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Hosting;

namespace Breezy.PantEX.ASP
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services
                .AddControllers()
                .SetCompatibilityVersion(CompatibilityVersion.Version_3_0);

            services
                .AddEntityFrameworkNpgsql()
                .AddDbContext<PantEXContext>(options => options.UseNpgsql(Configuration.GetConnectionString("Core")));

            services.AddSingleton<ImageUtil>();

            services.Configure<FileStorageOptions>(Configuration.GetSection("FileStorage"));
            services.AddSingleton<FileStorage.FileStorage>();

            services.Configure<ImageOptimizerOptions>(Configuration.GetSection("ImageOptimizer"));
            services.AddScoped<ImageOptimizerService>();
            services.AddHostedService<ImageOptimizer>();

            services
                .AddIdentity<IdentityUser, IdentityRole>(options =>
                {
                    options.Password.RequireDigit = false;
                    options.Password.RequireNonAlphanumeric = false;
                })
                .AddEntityFrameworkStores<PantEXContext>();

            services.AddSpaStaticFiles(configuration =>
            {
                configuration.RootPath = Path.Join("pantex-app", "build");
            });

            services.ConfigureApplicationCookie(options =>
            {
                options.LoginPath = "/#login";
                options.LogoutPath = "/#logout";
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseRouting();

            app.UseDefaultFiles();
            app.UseStaticFiles();
            app.UseStaticFiles(new StaticFileOptions
            {
                RequestPath = "/images",
                FileProvider = new PhysicalFileProvider(Configuration.GetSection("FileStorage").GetValue<string>("Path"))
            });
            app.UseSpaStaticFiles();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints => endpoints.MapControllers());

            app.UseSpa(spa =>
            {
                spa.Options.SourcePath = Path.Join(env.ContentRootPath, "pantex-app");
                if (env.IsDevelopment())
                {
                    spa.UseProxyToSpaDevelopmentServer("http://localhost:3000");
                }
            });
        }
    }
}
