﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace Breezy.PantEX.ASP.Models
{
    public partial class PantEXContext : IdentityDbContext
    {
        public PantEXContext()
        {
        }

        public PantEXContext(DbContextOptions<PantEXContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Attachment> Attachments { get; set; }
        public virtual DbSet<Post> Posts { get; set; }
        public virtual DbSet<PostToTag> PostToTag { get; set; }
        public virtual DbSet<PostToCollection> PostsToCollection { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Attachment>(entity =>
            {
                entity.ToTable("attachments", "core");

                entity.HasIndex(e => e.PostId)
                    .HasName("post_index");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasDefaultValueSql("nextval('attachments_id_seq')");

                entity.Property(e => e.LocalPath)
                    .IsRequired()
                    .HasColumnName("local_path");

                entity.Property(e => e.Mimetype)
                    .IsRequired()
                    .HasColumnName("mimetype");

                entity.Property(e => e.PostId).HasColumnName("post_id");

                entity.Property(e => e.Role).HasColumnName("role");

                entity.Property(e => e.VisualHeight).HasColumnName("visual_height");

                entity.Property(e => e.VisualWidth).HasColumnName("visual_width");

                entity.HasOne(d => d.Post)
                    .WithMany(p => p.Attachments)
                    .HasForeignKey(d => d.PostId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("attachments_post_fk");
            });

            modelBuilder.Entity<Post>(entity =>
            {
                entity.ToTable("posts", "core");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasDefaultValueSql("nextval('posts_id_seq')");

                entity.Property(e => e.Approved).HasColumnName("approved");

                entity.Property(e => e.Description).HasColumnName("description");
            });

            modelBuilder.Entity<PostToTag>(entity =>
            {
                entity.HasKey(e => new { e.PostId, e.Tag });

                entity.ToTable("post_to_tag", "core");

                entity.HasIndex(e => e.Tag)
                    .HasName("tag_index");

                entity.Property(e => e.PostId).HasColumnName("post_id");

                entity.Property(e => e.Tag).HasColumnName("tag");

                entity.HasOne(d => d.Post)
                    .WithMany(p => p.Tags)
                    .HasForeignKey(d => d.PostId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("post_to_tag_post_fk");
            });

            modelBuilder.Entity<PostToCollection>(entity =>
            {
                entity.HasKey(e => new { e.PostId, e.UserId, e.CollectionName });

                entity.ToTable("post_to_collection", "core");

                entity.HasIndex(e => new { e.UserId, e.CollectionName })
                    .HasName("post_to_collection_user_collection_index");

                entity.HasIndex(e => e.CollectionName)
                    .HasName("post_to_collection_collection_index");

                entity.Property(e => e.PostId).HasColumnName("post_id");

                entity.Property(e => e.UserId).HasColumnName("user_id");
                
                entity.Property(e => e.CollectionName).HasColumnName("collection_name");

                entity.Property(e => e.CreatedAt)
                    .HasColumnName("created_at")
                    .HasDefaultValueSql("now()");

                entity.HasOne(e => e.Post)
                    .WithMany(p => p.CollectionEntries)
                    .HasForeignKey(e => e.PostId)
                    .OnDelete(DeleteBehavior.SetNull)
                    .HasConstraintName("post_to_collection_post_fk");
            });

            modelBuilder.HasSequence("attachments_id_seq");

            modelBuilder.HasSequence("posts_id_seq");
        }
    }
}
