﻿using System;
using System.Text.Json.Serialization;

namespace Breezy.PantEX.ASP.Models
{
    public partial class Attachment
    {
        public long Id { get; set; }
        public long PostId { get; set; }
        public string Mimetype { get; set; }
        public string LocalPath { get; set; }
        public int? VisualWidth { get; set; }
        public int? VisualHeight { get; set; }
        public short Role { get; set; }

        [JsonIgnore]
        public Post Post { get; set; }
    }

    [Flags]
    public enum AttachmentRole
    {
        Source = 0x01,
        Preview = 0x02,
        Thumbnail = 0x04,
        AlternativeFormat = 0x08
    }
}
