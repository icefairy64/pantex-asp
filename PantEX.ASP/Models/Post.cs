﻿using System.Collections.Generic;

namespace Breezy.PantEX.ASP.Models
{
    public partial class Post
    {
        public Post()
        {
            Attachments = new HashSet<Attachment>();
            Tags = new HashSet<PostToTag>();
        }

        public long Id { get; set; }
        public string Description { get; set; }
        public bool Approved { get; set; }

        public ICollection<Attachment> Attachments { get; set; }
        public ICollection<PostToTag> Tags { get; set; }
        public ICollection<PostToCollection> CollectionEntries { get; set; }
    }
}
