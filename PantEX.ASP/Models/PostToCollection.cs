using System;
using Microsoft.AspNetCore.Identity;

namespace Breezy.PantEX.ASP.Models
{
    public class PostToCollection
    {
        public long PostId { get; set; }
        public Post Post { get; set; }
        public string UserId { get; set; }
        public IdentityUser User { get; set; }
        public string CollectionName { get; set; }
        public DateTime CreatedAt { get; set; }
    }
}