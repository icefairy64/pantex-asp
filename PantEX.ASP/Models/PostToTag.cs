﻿using System.Text.Json.Serialization;

namespace Breezy.PantEX.ASP.Models
{
    public partial class PostToTag
    {
        public long PostId { get; set; }
        public string Tag { get; set; }

        [JsonIgnore]
        public Post Post { get; set; }
    }
}
