using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mime;
using System.Threading;
using System.Threading.Tasks;
using Breezy.ContentUtil;
using Breezy.PantEX.ASP.Extensions;
using Breezy.PantEX.ASP.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace Breezy.PantEX.ASP.Services
{
    public class ImageOptimizer : BackgroundService
    {
        private readonly IOptionsMonitor<ImageOptimizerOptions> Options;
        private readonly IServiceProvider Services;

        public ImageOptimizer(IOptionsMonitor<ImageOptimizerOptions> options, IServiceProvider services)
        {
            Options = options;
            Services = services;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            while (!stoppingToken.IsCancellationRequested)
            {
                await DoRun(stoppingToken);
                Thread.Sleep(TimeSpan.FromMilliseconds(Options.CurrentValue.RunPeriod));
            }
        }

        private async Task DoRun(CancellationToken stoppingToken)
        {
            using var scope = Services.CreateScope();
            var service = scope.ServiceProvider.GetRequiredService<ImageOptimizerService>();
            await service.Optimize(stoppingToken);
        }
    }

    public class ImageOptimizerService
    {
        private readonly IOptionsMonitor<ImageOptimizerOptions> Options;
        private readonly ILogger<ImageOptimizerService> Logger;
        private readonly PantEXContext Context;
        private readonly ImageUtil ImageUtil;
        private readonly FileStorage.FileStorage FileStorage;

        public ImageOptimizerService(
            IOptionsMonitor<ImageOptimizerOptions> options, 
            ILogger<ImageOptimizerService> logger, 
            PantEXContext context, 
            ImageUtil imageUtil,
            FileStorage.FileStorage fileStorage)
        {
            Options = options;
            Logger = logger;
            Context = context;
            ImageUtil = imageUtil;
            FileStorage = fileStorage;
        }

        public async Task Optimize(CancellationToken stoppingToken)
        {
            try
            {
                Logger.LogInformation("Starting optimization");

                var currentOptions = Options.CurrentValue;

                foreach (var mime in currentOptions.Preview.Mimetypes)
                {
                    await GeneratePreviews(stoppingToken, AttachmentRole.Preview, currentOptions.BatchSize, currentOptions.Preview.Size, currentOptions.Preview.Directory, mime);
                }
                
                foreach (var mime in currentOptions.Thumbnail.Mimetypes)
                {
                    await GeneratePreviews(stoppingToken, AttachmentRole.Thumbnail, currentOptions.BatchSize, currentOptions.Thumbnail.Size, currentOptions.Thumbnail.Directory, mime);
                }

                Logger.LogInformation("Finished optimization");
            }
            catch (Exception e)
            {
                Logger.LogError(e, "Failed to optimize");
            }
        }

        private static IQueryable<Post> AddConvertibleFilter(IQueryable<Post> linq, string targetMime)
        {
            var targetMedia = targetMime.Substring(0, targetMime.IndexOf('/')).ToLower();
            return targetMedia switch
            {
                "image" => linq,
                "video" => linq
                    .Where(post => post.Attachments
                        .Single(a => ((AttachmentRole) a.Role).HasFlag(AttachmentRole.Source))
                        .Mimetype.StartsWith("video")),
                _ => throw new Exception($"Unsupported target mimetype: {targetMime}")
            };
        }
        
        private async Task GeneratePreviews(CancellationToken stoppingToken, AttachmentRole previewRole, int batchSize, int previewSize, string previewDir, string previewMime)
        {
            var selection = await Context.Posts
                .Include(post => post.Attachments)
                .Where(post => post.Attachments
                    .Select(a => a.VisualWidth != null && a.VisualHeight != null ? Math.Max(a.VisualWidth.Value, a.VisualHeight.Value) : (int?) null)
                    .Max().GetValueOrDefault() > previewSize)
                .Where(post => !post.Attachments.Any(attachment => ((AttachmentRole)attachment.Role).HasFlag(previewRole) && attachment.Mimetype == previewMime))
                .Map(linq => AddConvertibleFilter(linq, previewMime))
                .Take(batchSize)
                .ToListAsync(cancellationToken: stoppingToken);

            foreach (var post in selection)
            {
                if (stoppingToken.IsCancellationRequested) {
                    return;
                }

                Logger.LogInformation("Generating {Role} ({Mimetype}) for post {PostId}", previewRole, previewMime, post.Id);

                var source = post.Attachments
                    .Single(attachment => ((AttachmentRole)attachment.Role).HasFlag(AttachmentRole.Source)
                                          && !((AttachmentRole)attachment.Role).HasFlag(AttachmentRole.AlternativeFormat));

                var scale = previewSize / (float)Math.Max(source.VisualWidth.Value, source.VisualHeight.Value);
                var previewWidth = (int)(source.VisualWidth.Value * scale);
                var previewHeight = (int)(source.VisualHeight.Value * scale);

                var tmpFilePath = Path.GetTempFileName();

                try
                {
                    IContentProcessor processor;
                    await using (var sourceStream = FileStorage.GetFileStream(source.LocalPath))
                    {
                        processor = await ImageUtil.GetSupportingContentProcessorAsync(sourceStream);
                    }

                    if (processor.SupportsFiles)
                    {
                        var sourceFile = new FileInfo(FileStorage.GetFilePath(source.LocalPath));
                        var destFile = new FileInfo(tmpFilePath);
                        if (processor.SupportsAsync)
                            await processor.ConvertAsync(sourceFile, destFile, previewMime, previewWidth, previewHeight);
                        else
                            processor.Convert(sourceFile, destFile, previewMime, previewWidth, previewHeight);
                    }
                    else
                    {
                        await using var destStream = new FileStream(tmpFilePath, FileMode.Open);
                        await using var sourceStream = FileStorage.GetFileStream(source.LocalPath);
                        if (processor.SupportsAsync)
                            await processor.ConvertAsync(sourceStream, destStream, previewMime, previewWidth, previewHeight);
                        else
                            processor.Convert(sourceStream, destStream, previewMime, previewWidth, previewHeight);
                    }

                    var extension = processor.SuggestExtensions(previewMime)[0];

                    var relPath = await FileStorage.StoreFileDirect(tmpFilePath, Path.Combine(previewDir, Path.ChangeExtension(source.LocalPath, extension)), true);

                    post.Attachments.Add(new Attachment
                    {
                        LocalPath = relPath,
                        VisualWidth = previewWidth,
                        VisualHeight = previewHeight,
                        Mimetype = previewMime,
                        Role = (short)previewRole
                    });

                    await Context.SaveChangesAsync(stoppingToken);
                }
                catch (Exception e)
                {
                    Logger.LogError(e, "Failed to generate {Role} for post {PostId}", previewRole, post.Id);
                }
                finally
                {
                    File.Delete(tmpFilePath);
                }
            }

            Logger.LogInformation("Finished generating {Role}s ({Mimetype})", previewRole, previewMime);
        }
    }

    public class ImageOptimizerOptions
    {
        public OptimizedRoleConfig Preview { get; set; }
        public OptimizedRoleConfig Thumbnail { get; set; }
        public int RunPeriod { get; set; }
        public int BatchSize { get; set; }
    }

    public class OptimizedRoleConfig
    {
        public string Directory { get; set; }
        public int Size { get; set; }
        public List<string> Mimetypes { get; set; }
    }
}