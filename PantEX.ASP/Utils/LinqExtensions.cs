using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Breezy.PantEX.ASP.Utils
{
    public static class LinqExtensions
    {
        public static IQueryable<T> WhereNegated<T>(
            this IQueryable<T> query,
            Expression<Func<T, bool>> expression,
            bool negate
        )
        {
            if (!negate)
                return query.Where(expression);
            
            var negatedExpr = Expression.Not(expression.Body);
            var delegateType = typeof(Func<,>)
                .GetGenericTypeDefinition()
                .MakeGenericType(new[] {
                        typeof(T),
                        typeof(bool) 
                    }
                );
            var finalExpr = Expression.Lambda(delegateType, negatedExpr, expression.Parameters);
            var resultExpression = (Expression<Func<T, bool>>) finalExpr;

            return query.Where(resultExpression);
        }
    }
}