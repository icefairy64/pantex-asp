import * as React from 'react';
import { action, decorate, entries, flow, observable, runInAction, set } from 'mobx';
import { observer } from 'mobx-react';
import EventListener from './pantex/event';
import Post from './pantex/post';
import TextField from './ux/textfield';
import Checkbox from './ux/checkbox';
import ActionButton from './action-button';
import PantEX from './pantex';

class PostCollectionsView extends React.Component {
    constructor() {
        super(...arguments);
        this.store = observable({
            post: null,
            collections: {}
        });
    }
    
    componentDidMount() {
        this.eventListener = new EventListener({
            postCollectionsViewShowRequest: this.onShowRequest
        }, this);
    }
    
    componentWillUnmount() {
        this.eventListener.destroy();
    }
    
    loadCollections() {
        this.store.post.getCollections().then(collections => runInAction(() => {
            Post.getUserCollections().then(userCollections => runInAction(() => {
                userCollections = userCollections.map(x => x.name);
                this.store.collections = userCollections
                    .map(x => [x, collections.indexOf(x) >= 0])
                    .reduce((a, x) => ({ ...a, [x[0]]: x[1] }), {});
                this.props.showContainer();
            }));
        }));
    }
    
    onShowRequest(sender, post) {
        this.store.post = post;
        this.loadCollections();
    }

    onCheckboxChange(collection, value) {
        this.store.collections[collection] = value;
    }
    
    onCollectionCreate(name) {
        set(this.store.collections, {
            [name]: true
        });
    }

    async onSaveClick() {
        const newCollections = entries(this.store.collections)
            .filter(e => e[1])
            .map(e => e[0]);
        try {
            await this.store.post.updateCollections(newCollections);
            this.props.closeContainer();
        }
        catch (e) {
            PantEX.fireError(e);
        }
    }
    
    renderCollectionList() {
        return <div>
            { entries(this.store.collections).map(entry => {
                const [collection, present] = entry;
                return <Checkbox
                    key={collection}
                    value={present}
                    labelText={collection}
                    onChange={value => this.onCheckboxChange(collection, value)}/>;
            }) }
        </div>
    }

    render() {
        return <div>
            { this.renderCollectionList() }
            <TextField className='px-space-top' onSubmit={this.onCollectionCreate.bind(this)} placeholder='New collection'/>
            <div className='px-form-bottom-bar'>
                <ActionButton iconCls='fas fa-save' text='Save' onClick={this.onSaveClick.bind(this)}/>
            </div>
        </div>
    }
}

decorate(PostCollectionsView, {
    onShowRequest: action,
    onCheckboxChange: action,
    onCollectionCreate: action    
});

export default observer(PostCollectionsView);