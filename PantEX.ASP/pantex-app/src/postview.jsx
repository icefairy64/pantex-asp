import { observable, autorun, runInAction } from 'mobx';
import PantEX from "./pantex";
import { observer } from "mobx-react";
import React from "react"

import ActionButton from "./action-button";

import './css/postview.css'
import PostEditor from './posteditor';

class PostView extends React.Component {
    constructor() {
        super(...arguments);
        this.store = observable({
            displayedAttachment: null,
            editing: false
        });
    }

    componentDidMount() {
        autorun(() => {
            if (this.store.prevPost !== this.props.post) {
                this.store.displayedAttachment = null;
                this.store.prevPost = this.props.post;
            }
        });
        this.unNavLeft = PantEX.on('navigationleft', this.onNavLeft, this);
        this.unNavRight = PantEX.on('navigationright', this.onNavRight, this);
        this.unNavEsc = PantEX.on('navigationesc', this.onNavEsc, this);
    }

    componentWillUnmount() {
        this.unNavLeft();
        this.unNavRight();
        this.unNavEsc();
    }

    onNavLeft() {
        if (this.props.onPrev) {
            this.props.onPrev();
        }
    }

    onNavRight() {
        if (this.props.onNext) {
            this.props.onNext();
        }
    }

    onNavEsc() {
        if (this.store.editing) {
            this.onEditCancel();
        } else {
            if (this.props.onEsc) {
                this.props.onEsc();
            }
        }
    }

    onTagClick(tag) {
        PantEX.fireEvent('searchrequest', tag);
    }

    onFullSizeClick() {
        this.store.displayedAttachment = this.props.post.getSourceAttachment();
    }

    onEditClick() {
        runInAction(() => {
            this.store.editing = true;
        });
    }

    async onFavClick() {
        if (this.store.favouriting) {
            return;
        }
        this.store.favouriting = true;
        try {
            if (this.props.post.favourited) {
                await this.props.post.unfavourite();
            } else {
                await this.props.post.favourite();
            }
            this.store.favouriting = false;
        }
        catch (e) {
            PantEX.fireError(e, e => e);
            this.store.favouriting = false;
        }
    }

    onEditCancel() {
        runInAction(() => {
            this.store.editing = false;
        });
    }
    
    onEditSuccess(post) {
        runInAction(() => {
            this.store.editing = false;
            this.props.post.updateFrom(post);
        });
    }

    renderPreview(previewAttachment) {
        const previewUrl = previewAttachment.getUrl();

        const tagCmps = this.props.post.tags
            .map(x => <span
                className="px-post-tag px-actionable"
                key={x}
                onClick={() => this.onTagClick(x)}>
                    {x}
            </span>);
        
        const previewCmp = previewAttachment.isVideo()
            ? <video
                className="px-post-preview"
                src={previewUrl}
                poster={this.props.post.getPreviewAttachment(false).getUrl()}
                loop={true}
                controls={true} />
            : <>
                <img className="px-post-preview" src={previewUrl}/>
                <img className="px-post-preview px-post-preview-blurred" src={previewUrl}/>
            </>
        
        return <>
            <div className={`px-post-preview-container px-post-view-fit ${previewAttachment.isVideo() ? 'px-post-preview-container-video' : ''}`}>
                {previewCmp}
            </div>
            <div className="px-post-fade"/>
            <div className="px-post-details">
                <div className="px-action-panel">
                    <ActionButton
                        iconCls="fas fa-heart"
                        className="px-button-favourite px-button-icon-only"
                        onClick={this.onFavClick.bind(this)}/>
                    <ActionButton iconCls="fas fa-download" className="px-button-download px-button-icon-only"/>
                    <ActionButton iconCls="fas fa-minus-circle" className="px-button-report px-button-icon-only"/>
                    <ActionButton iconCls="fas fa-pencil-alt" className="px-button-edit px-button-icon-only"
                                  onClick={this.onEditClick.bind(this)}/>
                    {!this.store.displayedAttachment &&
                    <ActionButton iconCls="fas fa-search-plus" className="px-button-download px-button-icon-only"
                                  onClick={this.onFullSizeClick.bind(this)}/>}
                </div>
                <div className="px-post-description">
                    <h2 className="px-post-title">{this.props.post.description}</h2>
                    <div className="px-tag-container">
                        {tagCmps}
                    </div>
                </div>
            </div>
        </>;
    }
    
    render() {
        let preview = this.store.displayedAttachment;
        if (!preview) {
            preview = this.props.post.getPreviewAttachment()
        }
        
        return <div
            className={this.props.className ? ('px-post-view ' + this.props.className) : 'px-post-view'}
            style={{
                '--preview-width': preview.width,
                '--preview-height': preview.height
            }}
        >
            {!this.store.editing 
                ? this.renderPreview(preview) 
                : <PostEditor
                    className='px-post-view-fit'
                    post={this.props.post}
                    onCancel={this.onEditCancel.bind(this)}
                    onSuccess={this.onEditSuccess.bind(this)}
                />
            }
        </div>
    }
}

export default observer(PostView);