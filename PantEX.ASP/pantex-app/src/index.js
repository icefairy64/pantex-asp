import React from 'react';
import ReactDOM from 'react-dom';
import Page from './page';
import { observable } from 'mobx';

const store = observable({
    posts: [],
    title: '',
    logoUrl: '/resources/pantex-logo-1.png',
    galleryColRatio: 4
})

ReactDOM.render(<Page store={store}/>, document.getElementById('root'));