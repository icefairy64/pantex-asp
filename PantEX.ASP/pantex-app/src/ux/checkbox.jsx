import * as React from 'react';
import { appendPropsClasses } from '../util';
import { action, decorate, observable, runInAction } from 'mobx';
import { observer } from 'mobx-react';

import '../css/ux/checkbox.css';

class Checkbox extends React.Component {
    constructor() {
        super(...arguments);
        this.value = observable.box(this.props.value || false);
        this.fieldId = 'checkbox-' + Date.now();
    }
    
    componentDidUpdate(prevProps, prevState, snapshot) {
        if (prevProps.value !== this.props.value) {
            runInAction(() => {
                this.value.set(this.props.value);
            });
        }
    }

    onCheckboxChange(ev) {
        this.value.set(!this.value.get());
        if (this.props.onChange) {
            this.props.onChange(this.value.get());
        }
    }
    
    render() {
        return <div className={appendPropsClasses('px-checkbox', this.props)}>
            <input
                type='checkbox'
                id={this.fieldId}
                checked={this.value.get()}
                onChange={this.onCheckboxChange.bind(this)}/>
            <label htmlFor={this.fieldId}>{this.props.labelText}</label>
        </div>
    }
}

decorate(Checkbox, {
    onCheckboxChange: action
});

export default observer(Checkbox);