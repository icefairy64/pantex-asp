import React, { Component } from 'react';
import { observer } from 'mobx-react';
import { observable, runInAction } from 'mobx';
import { appendPropsClasses } from '../util';

import '../css/ux/textfield.css';

class TextField extends Component {
    constructor(props, context) {
        super(props, context);
        if (!this.props.observable) {
            this.standalone = true;
            this.value = observable.box(this.props.value || '');
        }
    }

    onInputChange(ev) {
        runInAction(() => {
            if (this.standalone) {
                this.value.set(ev.target.value);
            } else {
                this.props.observable[this.props.valueField] = ev.target.value;
            }
        });
    }

    onInputKeyDown(ev) {
        if (ev.key === 'Enter') {
            if (this.props.onSubmit) {
                this.props.onSubmit(this.value.get());
                runInAction(() => {
                    this.value.set('');
                });
            }
            ev.preventDefault();
        }
    }
    
    renderStandalone() {
        return <input
            className={appendPropsClasses('px-textfield', this.props)}
            value={this.value.get()}
            placeholder={this.props.placeholder}
            onChange={this.onInputChange.bind(this)}
            onKeyDown={this.onInputKeyDown.bind(this)}
        />
    }
    
    renderBound() {
        return <input
            className={appendPropsClasses('px-textfield', this.props)}
            value={this.props.observable[this.props.valueField]}
            placeholder={this.props.placeholder}
            onChange={this.onInputChange.bind(this)}
        />
    }

    render() {
        if (this.standalone) {
            return this.renderStandalone();
        } else {
            return this.renderBound();
        }
    }
}

export default observer(TextField);