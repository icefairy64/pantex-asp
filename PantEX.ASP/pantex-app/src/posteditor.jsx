import React, { Component } from 'react';
import { observer } from 'mobx-react';
import TextField from './ux/textfield';
import { appendPropsClasses } from './util';
import ActionButton from './action-button';
import PantEX from './pantex';

import './css/form.css';
import { runInAction } from 'mobx';

class PostEditor extends Component {
    constructor(props, context) {
        super(props, context);
        this.post = props.post.clone();
    }
    
    async onSaveClick() {
        try {
            await this.post.save();
            if (this.props.onSuccess) {
                this.props.onSuccess(this.post);
            }
        }
        catch (e) {
            PantEX.fireError(e);
        }
    }

    onTagClick(tag) {
        runInAction(() => {
            this.post.tags.remove(tag);
        });
    }

    onNewTagSubmit(tag) {
        runInAction(() => {
            this.post.tags.push(tag);
        });
    }
    
    render() {
        const tagCmps = this.post.tags
            .map(x => <span
                className="px-post-tag px-actionable"
                key={x}
                onClick={() => this.onTagClick(x)}>
                    {x}
            </span>);
        
        return <div className={appendPropsClasses('px-post-editor px-form', this.props)}>
            <div className='px-form-body'>
                <h1>Edit post</h1>
                <TextField className='px-space-top' observable={this.post} valueField='description' />
                <div className='px-space-top'>
                    {tagCmps}
                    <TextField className='px-short' onSubmit={this.onNewTagSubmit.bind(this)} />
                </div>
            </div>
            <div className='px-form-bottom-bar'>
                <ActionButton className='px-button-ok' iconCls='fas fa-save' text='Save' onClick={this.onSaveClick.bind(this)} />
                <ActionButton className='px-button-cancel' iconCls='fas fa-times' text='Cancel' onClick={this.props.onCancel} />
            </div>
        </div>;
    }
}

export default observer(PostEditor);