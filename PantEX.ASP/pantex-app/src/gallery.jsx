import { observer } from 'mobx-react'
import { observable, autorun, runInAction } from 'mobx'
import React from 'react'

import PantEX from './pantex'
import Post from "./pantex/post";

import Thumb from './thumb';
import WindowView from './window';
import PostView from './postview';

import './css/gallery.css'
import EventListener from "./pantex/event";

const Gallery = observer(
    class Gallery extends React.Component {
        constructor() {
            super(...arguments);
            this.store = observable({
                postOffset: 0,
                posts: [],
                loadRequired: true,
                currentPost: null,
                postViewClosed: false
            });
        }

        componentDidMount() {
            this.eventListener = new EventListener({
                scrollend: this.onScrollEnd,
                searchrequest: this.onSearchRequest
            }, this);
            this.unLoadRequestListener = autorun(() => {
                if (this.store.loadRequired && !this.store.loading && !this.store.streamFinished) {
                    this.store.loadRequired = false;
                    this.loadPosts();
                }
            });
        }

        componentWillUnmount() {
            this.eventListener.destroy();
            this.unLoadRequestListener();
        }

        componentDidUpdate(prevProps) {
            if (this.props.query !== prevProps.query) {
                if (!this.store.loading) {
                    this.updateQuery();
                } else {
                    const prevListener = this.chunkLoadListener;
                    this.chunkLoadListener = () => {
                        if (prevListener) {
                            prevListener();
                        }
                        this.updateQuery();
                    }
                }
            }
        }

        onScrollEnd() {
            if (!this.store.loading && !this.store.streamFinished) {
                runInAction(() => {
                    this.store.loadRequired = true
                });
            }
        }

        onSearchRequest() {
            runInAction(() => {
                this.store.postViewClosed = true;
            });
        }

        onChunkLoad() {
            if (this.chunkLoadListener) {
                this.chunkLoadListener();
            }
        }

        updateQuery() {
            runInAction(() => {
                this.store.postOffset = 0;
                this.store.posts = [];
                this.store.loadRequired = true;
                this.store.streamFinished = false;
            });
        }

        async loadPosts() {
            this.store.loading = true;
            try {
                const posts = await Post.query(this.store.postOffset || 0, 80, (this.props.query || '') + ' pantex:optimized');
                runInAction(() => {
                    this.store.postOffset += posts.length;
                    this.store.posts.push(...posts);
                    this.store.streamFinished = posts.length === 0;
                    this.store.loading = false;
                });
            }
            catch (e) {
                this.store.loading = false;
                PantEX.fireError(e, e => `[Gallery] Failed to load posts, error: ${e}`);
            }
            this.onChunkLoad();
        }

        onThumbClick(thumb) {
            runInAction(() => {
                this.store.currentPost = thumb.props.post;
            });
        }

        onPreviewClose() {
            runInAction(() => {
                this.store.currentPost = null;
                this.store.postViewClosed = false;
            });
        }

        onPostPrev() {
            const currentPostIndex = this.store.posts.findIndex(x => x === this.store.currentPost);
            if (currentPostIndex <= 0) {
                return;
            }
            runInAction(() => {
                this.store.currentPost = this.store.posts[currentPostIndex - 1];
            });
        }

        onPostNext() {
            const currentPostIndex = this.store.posts.findIndex(x => x === this.store.currentPost);
            if (currentPostIndex < 0 || currentPostIndex >= this.store.posts.length) {
                return;
            }
            runInAction(() => {
                this.store.currentPost = this.store.posts[currentPostIndex + 1];
            });
        }
        
        onPostEsc() {
            runInAction(() => {
                this.store.postViewClosed = true;
            });
        }

        renderThumbRows() {
            const targetRatio = this.props.colRatio;
            const padding = this.props.padding;

            const rows = [];

            function newRow() {
                rows.push({
                    thumbs: []
                });
            }

            function rowWidth(row) {
                const height = rowHeight(row);
                return row.thumbs.map(x => x.attachment).map(x => x.width * height / x.height).reduce((a, x) => a + x, 0) + padding * (row.thumbs.length - 1);
            }

            function rowWidthClean(row) {
                const height = rowHeight(row);
                return row.thumbs.map(x => x.attachment).map(x => x.width * height / x.height).reduce((a, x) => a + x, 0);
            }

            function rowHeight(row) {
                return row.thumbs.map(x => x.attachment).map(x => Math.max(x.width, x.height)).reduce((a, x) => Math.max(a, x), 0);
            }

            function pushThumb(post) {
                if (rows.length === 0) {
                    newRow();
                }

                const row = rows[rows.length - 1];

                const thumbAttachment = post.getThumbnailAttachment();
                const thumbCmp = <Thumb 
                    key={post.id} 
                    thumbAttachment={thumbAttachment}
                    post={post}
                    onClick={this.onThumbClick.bind(this)}
                    >
                </Thumb>

                row.thumbs.push({
                    attachment: thumbAttachment,
                    cmp: thumbCmp
                });

                if (rowWidth(row) / rowHeight(row) > targetRatio) {
                    newRow();
                }
            }

            this.store.posts.forEach(x => pushThumb.call(this, x));

            return rows.map((x, idx) => {
                return <div
                    key={idx}
                    className='px-gallery-row'
                    style={{
                        '--row-width': rowWidth(x),
                        '--row-width-clean': rowWidthClean(x),
                        '--row-height': rowHeight(x)
                    }}>
                    {x.thumbs.map(t => t.cmp)}
                </div>
            });
        }

        render() {
            return <div 
                className="px-gallery"
                style={{
                    '--padding': `${this.props.padding}px`
                }}>
                {this.renderThumbRows()}
                {this.store.currentPost && 
                    <WindowView 
                        fullscreen={true}
                        onClose={this.onPreviewClose.bind(this)}
                        closed={this.store.postViewClosed}>
                        <PostView 
                            post={this.store.currentPost}
                            onPrev={this.onPostPrev.bind(this)}
                            onNext={this.onPostNext.bind(this)}
                            onEsc={this.onPostEsc.bind(this)}/>
                    </WindowView>
                }
            </div>
        }
    }
);

export default Gallery;