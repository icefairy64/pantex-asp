import React from 'react'

class ActionButton extends React.Component {
    onClick(ev) {
        if (this.props.onClick) {
            this.props.onClick();
        }
        ev.stopPropagation();
        ev.preventDefault();
    }

    render() {
        return <button className={'px-action-button px-actionable ' + (this.props.className || '')} onClick={this.onClick.bind(this)}>
            {this.props.iconCls && <i className={this.props.iconCls}></i>}
            {this.props.text}
        </button>
    }
}

export default ActionButton;