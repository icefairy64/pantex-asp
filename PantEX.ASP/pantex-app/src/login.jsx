import React from 'react'

import PantEX from './pantex'

class Login extends React.Component {
    /** 
     * @param {React.ChangeEvent<HTMLInputElement>} e 
     */
    onUsernameChange(e) {
        this.setState({
            username: e.currentTarget.value
        });
    }

    /** 
     * @param {React.ChangeEvent<HTMLInputElement>} e 
     */
    onPasswordChange(e) {
        this.setState({
            password: e.currentTarget.value
        });
    }
    
    async onLoginClick() {
        try {
            await PantEX.Auth.login(this.state.username, this.state.password);
            if (this.props.onLogin) {
                this.props.onLogin();
            }
        }
        catch (e) {
            if (e instanceof Error) {
                e = e.message;
            }
            PantEX.fireEvent('error', `[Login] ${e}`);
        }
    }

    render() {
        return <div className="px-login">
            <div className="px-login-panel">
                <input placeholder="Username" onChange={this.onUsernameChange.bind(this)}></input>
                <input type="password" placeholder="Password" onChange={this.onPasswordChange.bind(this)}></input>
                <button onClick={this.onLoginClick.bind(this)}>Login</button>
            </div>
        </div>
    }
}

export default Login;