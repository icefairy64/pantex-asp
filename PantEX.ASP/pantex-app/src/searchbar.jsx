import React, {createRef} from 'react'

import PantEX from './pantex'

import './css/searchbar.css'
import {observer} from "mobx-react";
import {observable, runInAction} from "mobx";
import AutocompleteTask from "./pantex/autocomplete";
import EventListener from "./pantex/event";

const SuggestionList = observer(
    class SuggestionList extends React.Component {
        constructor(props) {
            super(props);
            this.store = observable({
                activeIndex: 0
            });
        }

        componentDidMount() {
            this.eventListener = new EventListener({
                suggestiondown: () => this.onSuggestionMove(1),
                suggestionup: () => this.onSuggestionMove(-1),
                suggestionchoice: this.onSuggestionChoice
            }, this);
        }
        
        componentWillUnmount() {
            this.eventListener.destroy();
        }
        
        onSuggestionMove(delta) {
            runInAction(() => {
                const targetIndex = this.store.activeIndex + delta;
                if (targetIndex >= 0 && targetIndex < this.props.suggestions.length) {
                    this.store.activeIndex = targetIndex;
                }
            });
        }
    
        onSuggestionClick(suggestion) {
            if (this.props.onSuggestionClick) {
                this.props.onSuggestionClick(suggestion);
            }
        }
        
        onSuggestionChoice() {
            this.onSuggestionClick(this.props.suggestions[this.store.activeIndex]);
        }
        
        render() {
            return <ul className="px-search-suggestion-box">
                { this.props.suggestions.map((suggestion, index) => {
                    return <li
                        className={`px-search-suggestion-item${index === this.store.activeIndex ? ' px-active' : ''}`}
                        key={index}
                        onClick={() => this.onSuggestionClick(suggestion)}>
                        {suggestion.suggestion}
                    </li>
                }) }
            </ul>
        }
    }
)

const SearchBar = observer(
    class SearchBar extends React.Component {
        constructor(props, context) {
            super(props, context);
            this.inputBox = createRef();
            this.suggestions = observable([]);
            this.store = observable({
                query: ''
            });
        }
        
        componentDidMount() {
            this.autocompleteTask = new AutocompleteTask(this.onSuggestionComplete.bind(this), 500);
        }
        
        componentWillUnmount() {
            this.autocompleteTask.cancel();
        }

        /**
         * @param {React.ChangeEvent<HTMLInputElement>} ev
         */
        onInputChange(ev) {
            runInAction(() => {
                this.store.query = ev.target.value
            });
        }
    
        /**
         * @param {React.KeyboardEvent<HTMLInputElement>} ev
         */
        onInputKeyDown(ev) {
            if (ev.key === 'Enter') {
                if (this.suggestions.length > 0) {
                    PantEX.fireEvent('suggestionchoice', this);
                } else {
                    this.onSearch();
                    runInAction(() => {
                        this.store.query = '';
                    });
                }
            }
            if (ev.key === 'Escape') {
                this.autocompleteTask.cancel();
                runInAction(() => {
                    this.suggestions.clear();
                });
                ev.preventDefault();
                return;
            }
            if (ev.key === 'ArrowDown' || ev.key === 'ArrowUp') {
                PantEX.fireEvent(`suggestion${ev.key === 'ArrowDown' ? 'down' : 'up'}`, this);
                ev.preventDefault();
                return;
            }
            runInAction(() => {
                this.suggestions.clear();
            });
        }
        
        onInputKeyUp(ev) {
            if (ev.key === 'ArrowDown' || ev.key === 'ArrowUp' || ev.key === 'Escape') {
                return;
            }
            const tokens = ev.target.value.split(' ');
            if (tokens.length > 0) {
                const lastToken = tokens[tokens.length - 1];
                this.autocompleteTask.update(lastToken);
            }
            runInAction(() => {
                this.suggestions.clear();
            });
        }
    
        onSearch() {
            if (this.onSearch) {
                this.props.onSearch(this.store.query);
            } else {
                PantEX.fireEvent('searchrequest', this.store.query);
            }
        }

        onSuggestionComplete(suggestions) {
            runInAction(() => {
                this.suggestions.replace(suggestions);
            });
        }

        onSuggestionClick(suggestion) {
            runInAction(() => {
                this.suggestions.clear();
                const query = this.store.query;
                const tokens = query.split(' ');
                if (tokens.length > 0) {
                    tokens[tokens.length - 1] = suggestion.suggestion;
                }
                this.store.query = tokens.join(' ') + ' ';
                this.inputBox.current.focus();
            });
        }
    
        render() {
            return <div className="px-search-box">
                    <input 
                    type="search" 
                    placeholder="Search"
                    ref={this.inputBox}
                    autoComplete="false"
                    onChange={this.onInputChange.bind(this)}
                    onKeyDown={this.onInputKeyDown.bind(this)}
                    onKeyUp={this.onInputKeyUp.bind(this)}
                    value={this.store.query}
                />
                { this.suggestions.length > 0 && <SuggestionList
                    suggestions={this.suggestions}
                    onSuggestionClick={this.onSuggestionClick.bind(this)}/> }
            </div>
        }
    }
)

export default SearchBar;