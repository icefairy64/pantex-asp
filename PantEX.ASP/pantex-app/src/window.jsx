import React from 'react'
import { appendPropsClasses } from './util';
import EventListener from './pantex/event';

function windowedComponent(closeOnEsc) {
    return function (TComponent) {
        const clazz = class WindowedComponent extends React.Component {
            constructor(props) {
                super(...arguments);
                this.state = {
                    windowClosed: props.closed || false
                };
            }

            componentDidUpdate(prevProps, prevState, snapshot) {
                if (prevProps.closed !== this.props.closed) {
                    this.setState({
                        windowClosed: this.props.closed
                    });
                }
            }

            closeWindow() {
                this.setState({
                    windowClosed: true
                });
            }

            showWindow() {
                this.setState({
                    windowClosed: false
                });
            }

            onWindowClose() {
                if (this.props.onWindowClose) {
                    this.props.onWindowClose(...arguments);
                }
                this.setState({
                    windowClosed: true
                });
            }

            render() {
                return <WindowView
                    closed={this.state.windowClosed}
                    className={this.props.windowClassName}
                    closeOnEsc={closeOnEsc}
                    onBeforeClose={this.props.onBeforeWindowClose}
                    onClose={this.onWindowClose.bind(this)}>
                    <TComponent
                        closeContainer={this.closeWindow.bind(this)}
                        showContainer={this.showWindow.bind(this)}
                        {...this.props} />
                </WindowView>
            }
        }
        clazz.displayName = `Windowed(${TComponent.name})`;
        return clazz;
    }
}

class WindowView extends React.Component {
    constructor(props) {
        super(...arguments);
        this.state = {
            closing: props.closed
        };
    }
    
    componentDidMount() {
        if (this.props.closeOnEsc) {
            this.eventListener = new EventListener({
                navigationesc: this.onCloseClick
            }, this);
        }
    }
    
    componentWillUnmount() {
        if (this.eventListener) {
            this.eventListener.destroy();
        }
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (prevProps.closed !== true && this.props.closed === true) {
            this.onCloseClick();
        }
        if (prevProps.closed === true && this.props.closed !== true) {
            this.show();
        }
    }

    onCloseClick() {
        if (this.props.onBeforeClose) {
            if (this.props.onBeforeClose() === false) {
                return;
            }
        }
        this.setState({
            closing: true
        });
    }
    
    show() {
        this.setState({
            closing: false
        });
    }

    onAnimationEnd(ev) {
        if (ev.animationName === 'px-disappear') {
            if (this.props.onClose) {
                this.props.onClose();
            }
        }
    }

    render() {
        const ownClassName = 'px-window' + (this.state.closing ? ' px-disappearing' : '') + (this.props.fullscreen ? ' px-fullscreen' : '');
        return <div
            className={appendPropsClasses(ownClassName, this.props)} 
            onAnimationEnd={this.onAnimationEnd.bind(this)}>
            
            {this.props.children}
            <button
                className="px-window-button px-icon-button fas fa-times"
                onClick={this.onCloseClick.bind(this)}/>
        </div>
    }
}

export default WindowView;
export { windowedComponent };