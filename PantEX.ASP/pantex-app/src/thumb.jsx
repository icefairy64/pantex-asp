import React from 'react'
import { observable, autorun, runInAction } from 'mobx'
import { observer } from 'mobx-react';

import PantEX from './pantex'

class Thumb extends React.Component {
    constructor() {
        super(...arguments);
        this.store = observable({});
    }

    onClick() {
        if (this.props.onClick) {
            this.props.onClick(this);
        }
    }

    async onFavClick(ev) {
        ev.stopPropagation();
        if (this.store.favouriting) {
            return;
        }
        this.store.favouriting = true;
        try {
            if (this.props.post.favourited) {
                await this.props.post.unfavourite();
            } else {
                await this.props.post.favourite();
            }
            this.store.favouriting = false;
        }
        catch (e) {
            PantEX.fireError(e, e => e);
            this.store.favouriting = false;
        }
    }
    
    onCollectionsClick(ev) {
        ev.stopPropagation();
        PantEX.fireEvent('postCollectionsViewShowRequest', this, this.props.post);
    }
    
    renderVideo(children) {
        return <div
            className="px-thumb px-thumb-video"
            data-postid={this.props.post.id}
            style={{
                '--height': this.props.thumbAttachment.height,
                '--width': this.props.thumbAttachment.width
            }}>
            <video
                src={this.props.thumbAttachment.getUrl()}
                loop={true}
                autoPlay={true}
                onClick={this.onClick.bind(this)}
            />
            {children}
        </div>
    }
    
    renderImage(children) {
        return <div
            className="px-thumb"
            data-postid={this.props.post.id}
            style={{
                backgroundImage: `url("${this.props.thumbAttachment.getUrl()}")`,
                '--height': this.props.thumbAttachment.height,
                '--width': this.props.thumbAttachment.width
            }}
            onClick={this.onClick.bind(this)}
        >
            {children}
        </div>;
    }

    render() {
        const childrenFragment = <>
            <div
                className="px-thumb-blurred"                
                style={{
                    backgroundImage: `url("${this.props.thumbAttachment.getUrl()}")`
                }}>
            </div>
            <div className="px-thumb-bar">
                <button className={`px-icon-button px-actionable fas fa-heart${this.props.post.favourited ? ' px-fav-icon-active' : ''}`} onClick={this.onFavClick.bind(this)}/>
                <button className='px-icon-button px-actionable fas fa-plus' onClick={this.onCollectionsClick.bind(this)}/>
            </div>
        </>
        
        if (this.props.thumbAttachment.isVideo()) {
            return this.renderVideo(childrenFragment);
        } else {
            return this.renderImage(childrenFragment);
        }
    }
}

export default observer(Thumb);