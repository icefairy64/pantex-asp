export function appendPropsClasses(staticClasses, props) {
    if (props.className) {
        return `${staticClasses} ${props.className}`;
    } else {
        return staticClasses;
    }
}