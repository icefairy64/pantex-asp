import { observer } from 'mobx-react'
import { runInAction } from 'mobx'
import React from 'react'

import PantEX from './pantex'
import Router from './pantex/router'
import Auth from './pantex/auth'
import DelayedTask from './pantex/delayed-task'

import Login from './login'
import Gallery from './gallery'
import SearchBar from './searchbar'

import './css/page.css'
import './thirdparty/fontawesome/css/all.min.css'
import { windowedComponent } from './window';
import PostCollectionsView from './postcollections';

const PostCollectionsWindow = windowedComponent(true)(PostCollectionsView);

const Page = observer(
    class Page extends React.Component {
        componentDidMount() {
            this.defaultRoute = 'posts';
            this.registerRoutes();
            this.initEventListeners();
            this.initScrollListeners();
            this.initKeyboardListeners();
            this.initApp();
        }

        registerRoutes() {
            const routeHandlerConfig = {
                fn: this.onRoute,
                scope: this
            };

            Router.init();
            
            Router.addRoute('login', {
                ...routeHandlerConfig,
                fn: this.onLoginRoute,
                name: 'login'
            });

            Router.addRoute('posts', {
                ...routeHandlerConfig,
                fn: this.onPostsRoute,
                name: 'posts'
            });

            Router.addRoute('search/(.*?)$', {
                ...routeHandlerConfig,
                fn: this.onSearchRoute,
                name: 'search'
            });

            Router.addRoute('', {
                ...routeHandlerConfig,
                fn: this.onHomeRoute,
                name: 'home'
            });
        }

        initEventListeners() {
            PantEX.on('error', this.onAppError, this);
            PantEX.on('routerequest', this.onRouteChangeRequest, this);
            PantEX.on('userdatareceive', this.onUserData, this);
            PantEX.on('searchrequest', this.onSearch, this);
        }

        initScrollListeners() {
            document.body.onscroll = () => {
                const body = document.body.parentElement;
                if (body.scrollTop + body.clientHeight >= body.scrollHeight - 200) {
                    if (!this.scrollEndTask) {
                        this.scrollEndTask = new DelayedTask(() => {
                            PantEX.fireEvent('scrollend');
                            delete this.scrollEndTask;
                        }, 10);
                    }
                    this.scrollEndTask.delay();
                }
            };
        }

        initKeyboardListeners() {
            document.body.addEventListener('keydown', ev => {
                if (ev.key === 'ArrowLeft') {
                    PantEX.fireEvent('navigationleft');
                } else if (ev.key === 'ArrowRight') {
                    PantEX.fireEvent('navigationright');
                } else if (ev.key === 'Escape') {
                    PantEX.fireEvent('navigationesc');
                }
            });
        }

        async initApp() {
            try {
                const userData = await Auth.loadUserData();
                if (!userData) {
                    PantEX.fireEvent('routerequest', 'login');
                } else {
                    Router.callHandlers();
                }
            }
            catch (e) {
                if (e instanceof Error) {
                    e = e.message;
                }
                PantEX.fireEvent('error', `[Page] ${e}`);
            }
        }

        onRoute(route) {
            runInAction(() => {
                this.props.store.currentRoute = route.name;
            });
        }

        onLoginRoute(route) {
            if (!this.props.store.userData) {
                runInAction(() => {
                    const routeDesc = PantEX.Router.getRouteDesc();
                    this.props.store.initialRoute = routeDesc === '' ? null : routeDesc;
                    this.onRoute(...arguments);
                });
            } else {
                PantEX.fireEvent('routerequest', this.defaultRoute);
            }
        }

        onHomeRoute() {
            PantEX.fireEvent('routerequest', this.defaultRoute);
        }

        onPostsRoute(route) {
            runInAction(() => {
                this.props.store.postQuery = '';
                this.onRoute(route);
            });
        }

        onSearchRoute(route, matches) {
            runInAction(() => {
                this.props.store.currentRoute = 'posts';
                this.props.store.postQuery = matches[0];
            });
        }

        onAppError(err) {
            if (typeof err === 'string') {
                err = {
                    msg: err
                };
            }
            err = { ...err };
            console.error(err.msg);
        }

        onRouteChangeRequest(routeDesc) {
            if (document.location.hash === '#' + routeDesc) {
                Router.callHandlers();
            }
            else {
                document.location.hash = '#' + routeDesc;
            }
        }

        onUserData(userData) {
            runInAction(() => {
                this.props.store.userData = userData;
            });
        }

        onLogin() {
            PantEX.fireEvent('routerequest', this.props.store.initialRoute || this.defaultRoute);
        }

        onSearch(query) {
            if (this.props.store.currentRoute === 'posts') {
                PantEX.fireEvent('routerequest', `search/${query.trim()}`);
            }
        }

        render() {
            let el;

            switch (this.props.store.currentRoute) {
                case 'login':
                    el = <Login onLogin={this.onLogin.bind(this)}/>
                    break;
                case 'posts':
                    el = <Gallery colRatio={this.props.store.galleryColRatio} padding={12} query={this.props.store.postQuery}/>;
                    break;
                default: 
                    el = <div>No route</div>;
                    break;
            }

            return <div>
                <div className="px-page-header">
                    {this.props.store.logoUrl && <img className="px-page-logo" src={this.props.store.logoUrl}/>}
                    <div className="px-page-title">{this.props.store.title}</div>
                    <a href="#posts">Posts</a>
                    <a href="#search/pantex:favourite">Favourites</a>
                    <div className="px-center-container">
                        <SearchBar onSearch={this.onSearch.bind(this)} />
                    </div>
                    {this.props.store.userData && <span className="px-user-name">{this.props.store.userData.userName}</span>}
                </div>
                <div className="px-page-body">
                    {el}
                    <PostCollectionsWindow 
                        closed={true} />
                </div>
            </div>
        }
    }
);

export default Page;