class DelayedTask {
    constructor(fn, timeout) {
        this.fn = fn;
        this.timeout = timeout;
    }
    delay(timeout) {
        if (this.handle) {
            window.clearTimeout(this.handle);
        }
        this.handle = window.setTimeout(this.fn, timeout || this.timeout);
    }
    cancel() {
        if (this.handle) {
            window.clearTimeout(this.handle);
        }
    }
}

export default DelayedTask;