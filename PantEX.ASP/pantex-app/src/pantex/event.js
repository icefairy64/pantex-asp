import PantEX from "../pantex";

class EventListener {
    constructor(descriptor, scope) {
        if (descriptor.scope && !scope) {
            scope = descriptor.scope;
            delete descriptor.scope;
        }
        this.handlers = Object.getOwnPropertyNames(descriptor).map(eventName => {
            return PantEX.on(eventName, descriptor[eventName], scope);
        });
    }
    
    destroy() {
        this.handlers.forEach(handler => handler());
    }
}

export default EventListener;