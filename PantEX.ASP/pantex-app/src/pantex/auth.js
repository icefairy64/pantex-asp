import PantEX from '.'

const Auth = {
    login: async function (username, password) {
        const form = new FormData();
        form.append('username', username);
        form.append('password', password);
        const response = await fetch(PantEX.urlRoot +'/api/auth/login', {
            method: 'POST',
            body: form,
            cache: 'no-cache'
        });
        if (response.ok) {
            this.loadUserData();
            return;
        } else {
            throw new Error(`Failed to log in, error: ${await response.text()}`);
        }
    },
    loadUserData: async function () {
        if (PantEX.noAuth) {
            return {};
        }
        const response = await fetch(PantEX.urlRoot + '/api/auth/info', {
            cache: 'no-cache'
        });
        if (response.ok) {
            const userData = await response.json();
            PantEX.fireEvent('userdatareceive', userData);
            return userData;
        } else if (response.redirected) {
            const url = decodeURIComponent(response.url);
            return null;
        } else {
            throw new Error(`Failed to load user data, error: ${await response.text()}`);
        }
    }
}

export default Auth;