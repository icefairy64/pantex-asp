const PantEX = {
    urlRoot: '',
    noAuth: false,
    
    listeners: {},
    
    fireEvent: function (eventName, ...args) {
        console.log(`[Event ${eventName}]`, ...args);
        const listeners = this.listeners[eventName];
        let result = true;
        if (listeners) {
            for (const listener of listeners) {
                result &= (listener.fn.call(listener.scope, ...args) !== false);
                if (!result) {
                    break;
                }
            }
        }
        return result;
    },
    on: function (eventName, handler, scope) {
        if (!this.listeners[eventName]) {
            this.listeners[eventName] = [];
        }
        const listeners = this.listeners[eventName];
        const listenerDesc = {
            fn: handler,
            scope
        };
        listeners.push(listenerDesc);
        return () => {
            this.listeners[eventName] = this.listeners[eventName].filter(x => x !== listenerDesc);
        }
    },
    fireError: function (error, fmt) {
        if (error.msg) {
            error = error.msg;
        }
        if (error.message) {
            error = error.message;
        }
        this.fireEvent('error', fmt ? fmt(error) : error);
    }
}

export default PantEX;