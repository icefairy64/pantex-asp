import {decorate, observable} from "mobx";
import PantEX from './index.js';

const PostStatic = {
    favouritesCollectionName: 'Favourites',
    /** @returns {Promise<Array<Post>>} */
    query: async function (offset, count, query) {
        let url = `${PantEX.urlRoot}/api/posts?offset=${offset || 0}&count=${count || 20}`;
        if (query) {
            url += `&query=${query}`;
        }
        const response = await fetch(url);
        return (await response.json()).map(x => new Post(x));
    },
    favourite: async function (post) {
        if (!post) {
            throw new Error('Post is null');
        }
        const url = `${PantEX.urlRoot}/api/collections/${this.favouritesCollectionName}/${post.id}`;
        const response = await fetch(url, {
            method: 'POST'
        });
        if (!response.ok) {
            throw new Error(`Failed to favourite post, reason: ${await response.text()}`);
        } else {
            post.favourited = true;
        }
    },
    unfavourite: async function (post) {
        if (!post) {
            throw new Error('Post is null');
        }
        const url = `${PantEX.urlRoot}/api/collections/${this.favouritesCollectionName}/${post.id}`;
        const response = await fetch(url, {
            method: 'DELETE'
        });
        if (!response.ok) {
            throw new Error(`Failed to unfavourite post, reason: ${await response.text()}`);
        } else {
            post.favourited = false;
        }
    },
    update: async function (post) {
        const url = `${PantEX.urlRoot}/api/posts/${post.id}`;
        const response = await fetch(url, {
            method: 'PATCH',
            body: JSON.stringify(post.getData()),
            headers: {
                'Content-Type': 'application/json'
            }
        });
        if (!response.ok) {
            throw new Error(`Failed to update post, reason: ${await response.text()}`);
        }
    },
    getCollections: async function (post) {
        const url = `${PantEX.urlRoot}/api/collections/post/${post.id}`;
        const response = await fetch(url);
        if (!response.ok) {
            throw new Error(`Failed to get post collections, reason: ${await response.text()}`);
        }
        return await response.json();
    },
    updateCollections: async function (post, collections) {
        const url = `${PantEX.urlRoot}/api/collections/post/${post.id}`;
        const response = await fetch(url, {
            method: 'PATCH',
            body: JSON.stringify(collections),
            headers: {
                'Content-Type': 'application/json'
            }
        });
        if (!response.ok) {
            throw new Error(`Failed to update post collections, reason: ${await response.text()}`);
        }
    },
    getUserCollections: async function () {
        const url = `${PantEX.urlRoot}/api/collections/`;
        const response = await fetch(url);
        if (!response.ok) {
            throw new Error(`Failed to get user collections, reason: ${await response.text()}`);
        }
        return await response.json();
    }
}

class Post {
    constructor(data) {
        this.description = data.description;
        this.tags = data.tags;
        this.id = data.id;
        /** @type {Array<Attachment>} */
        this.attachments = data.attachments.map(x => new Attachment(x));
        this.favourited = data.favourited || false;
    }

    getSourceAttachment() {
        return this.attachments.find(x => x.isSource);
    }

    getPreviewAttachment(includeVideo) {
        const previews = this.attachments.filter(x => !x.isSource && x.getSize() >= 1024);
        if (includeVideo !== false) {
            let result = previews.find(x => x.isVideo());
            if (!result && this.getSourceAttachment().isVideo()) {
                return this.getSourceAttachment();
            } else {
                return previews[0] || this.getSourceAttachment();
            }
        } else {
            return previews[0] || this.getSourceAttachment();
        }
    }

    getThumbnailAttachment(includeVideo) {
        const thumbs = this.attachments.filter(x => !x.isSource && x.getSize() < 1024);
        if (includeVideo !== false) {
            return thumbs.find(x => x.isVideo()) || thumbs[0] || this.getSourceAttachment();
        } else {
            return thumbs[0] || this.getSourceAttachment();
        }
    }

    favourite() {
        return PostStatic.favourite(this);
    }

    unfavourite() {
        return PostStatic.unfavourite(this);
    }
    
    getData() {
        return {
            id: this.id,
            tags: this.tags.slice(),
            description: this.description,
            attachments: this.attachments.map(x => x.data),
            favourited: this.favourited
        };
    }
    
    clone() {
        return new Post(this.getData());
    }
    
    save() {
        return PostStatic.update(this);
    }
    
    updateFrom(post) {
        this.tags = post.tags;
        this.description = post.description;
    }
    
    getCollections() {
        return PostStatic.getCollections(this);
    }
    
    updateCollections(collections) {
        return PostStatic.updateCollections(this, collections);
    }
}

decorate(Post, {
    favourited: observable,
    description: observable,
    tags: observable
});

class Attachment {
    constructor(data) {
        this.width = data.width;
        this.height = data.height;
        this.isSource = data.role & 1 === 1;
        this.isAlternative = data.role & 8 === 8;
        this.isPreview = data.role & 2 === 2 || data.role & 4 === 4;
        this.localPath = data.localPath;
        this.mimetype = data.mimetype;
        this.data = Object.assign({}, data);
    }

    getUrl() {
        return PantEX.urlRoot + '/images/' + this.localPath;
    }

    getSize() {
        return Math.max(this.width, this.height);
    }
    
    isVideo() {
        return this.mimetype.indexOf('video') === 0;
    }
}

export default PostStatic;
export {
    Post,
    Attachment
};