import DelayedTask from "./delayed-task";
import PantEX from "./index";

class AutocompleteTask extends DelayedTask {
    constructor(callback, timeout) {
        super(async () => {
            this.result = await this.fetchSuggestions();
            callback.call(this, this.result);
        }, timeout);
        this.result = [];
    }

    update(query) {
        this.query = query;
        this.delay();
    }
    
    async fetchSuggestions() {
        const response = await fetch(`${PantEX.urlRoot}/api/search/autocomplete?part=${this.query}`);
        return await response.json();
    }
}

export default AutocompleteTask;