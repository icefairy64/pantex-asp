import PantEX from ".";

const Router = {
    routes: {},

    init: function () {
        window.addEventListener('hashchange', this.onHashChange.bind(this));
    },

    onHashChange: function () {
        PantEX.fireEvent('hashchange', document.location.hash);
        const route = this.getRoute(document.location.hash);
        if (route) {
            route.route.fn.call(route.route.scope, route.route, route.matches);
        }
    },

    getRoute: function (hash) {
        const hashRoute = hash.substring(1);
        for (const routeDesc in this.routes) {
            if (routeDesc !== '') {
                const regexpMatches = new RegExp(routeDesc).exec(hashRoute)
                if (regexpMatches) {
                    return {
                        route: this.routes[routeDesc],
                        matches: regexpMatches.slice(1)
                    };
                }
            }
            if (routeDesc === '' && hashRoute === routeDesc) {
                return {
                    route: this.routes[routeDesc],
                    matches: null
                };
            }
        }
        return null;
    },

    addRoute: function (routeDesc, handler, scope) {
        if (typeof handler === 'function') {
            this.routes[routeDesc] = {
                fn: handler,
                scope
            };
        } else {
            this.routes[routeDesc] = handler;
        }
    },

    callHandlers: function () {
        this.onHashChange();
    },

    getRouteDesc: function () {
        return document.location.hash.substring(Math.min(document.location.hash.length, 1));
    }
};

export default Router;