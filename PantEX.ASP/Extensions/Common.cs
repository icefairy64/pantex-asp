﻿using System;
namespace Breezy.PantEX.ASP.Extensions
{
    public static class Common
    {
        public static R Map<T, R>(this T obj, Func<T, R> mapper)
        {
            return mapper(obj);
        }

        public static Func<T, R2> Then<T, R, R2>(this Func<T, R> func, Func<R, R2> continuation)
        {
            return (T value) => continuation(func(value));
        }
    }
}
