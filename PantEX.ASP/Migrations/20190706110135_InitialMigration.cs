﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace PantEX.ASP.Migrations
{
    public partial class InitialMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "core");

            migrationBuilder.CreateSequence(
                name: "attachments_id_seq");

            migrationBuilder.CreateSequence(
                name: "posts_id_seq");

            migrationBuilder.CreateTable(
                name: "posts",
                schema: "core",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false, defaultValueSql: "nextval('posts_id_seq')"),
                    description = table.Column<string>(nullable: true),
                    approved = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_posts", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "attachments",
                schema: "core",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false, defaultValueSql: "nextval('attachments_id_seq')"),
                    post_id = table.Column<long>(nullable: false),
                    mimetype = table.Column<string>(nullable: false),
                    local_path = table.Column<string>(nullable: false),
                    visual_width = table.Column<int>(nullable: true),
                    visual_height = table.Column<int>(nullable: true),
                    role = table.Column<short>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_attachments", x => x.id);
                    table.ForeignKey(
                        name: "attachments_post_fk",
                        column: x => x.post_id,
                        principalSchema: "core",
                        principalTable: "posts",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "post_to_tag",
                schema: "core",
                columns: table => new
                {
                    post_id = table.Column<long>(nullable: false),
                    tag = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_post_to_tag", x => new { x.post_id, x.tag });
                    table.ForeignKey(
                        name: "post_to_tag_post_fk",
                        column: x => x.post_id,
                        principalSchema: "core",
                        principalTable: "posts",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "post_index",
                schema: "core",
                table: "attachments",
                column: "post_id");

            migrationBuilder.CreateIndex(
                name: "tag_index",
                schema: "core",
                table: "post_to_tag",
                column: "tag");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "attachments",
                schema: "core");

            migrationBuilder.DropTable(
                name: "post_to_tag",
                schema: "core");

            migrationBuilder.DropTable(
                name: "posts",
                schema: "core");

            migrationBuilder.DropSequence(
                name: "attachments_id_seq");

            migrationBuilder.DropSequence(
                name: "posts_id_seq");
        }
    }
}
