﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace PantEX.ASP.Migrations
{
    public partial class AddCollections : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "post_to_collection",
                schema: "core",
                columns: table => new
                {
                    post_id = table.Column<long>(nullable: false),
                    user_id = table.Column<string>(nullable: false),
                    collection_name = table.Column<string>(nullable: false),
                    created_at = table.Column<DateTime>(nullable: false, defaultValueSql: "now()")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_post_to_collection", x => new { x.post_id, x.user_id, x.collection_name });
                    table.ForeignKey(
                        name: "post_to_collection_post_fk",
                        column: x => x.post_id,
                        principalSchema: "core",
                        principalTable: "posts",
                        principalColumn: "id",
                        onDelete: ReferentialAction.SetNull);
                    table.ForeignKey(
                        name: "FK_post_to_collection_AspNetUsers_user_id",
                        column: x => x.user_id,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "post_to_collection_collection_index",
                schema: "core",
                table: "post_to_collection",
                column: "collection_name");

            migrationBuilder.CreateIndex(
                name: "post_to_collection_user_collection_index",
                schema: "core",
                table: "post_to_collection",
                columns: new[] { "user_id", "collection_name" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "post_to_collection",
                schema: "core");
        }
    }
}
