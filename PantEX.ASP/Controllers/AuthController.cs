using System.ComponentModel.DataAnnotations;
using System.Security.Claims;
using System.Threading.Tasks;
using Breezy.PantEX.ASP.Models;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Breezy.PantEX.ASP.Controllers
{
    [Route("api/auth")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private readonly PantEXContext PantEXContext;
        private readonly UserManager<IdentityUser> UserManager;
        private readonly SignInManager<IdentityUser> SignInManager;
        private readonly ILogger<AuthController> Logger;

        public AuthController(PantEXContext pantExContext, UserManager<IdentityUser> userManager, SignInManager<IdentityUser> signInManager, ILogger<AuthController> logger)
        {
            PantEXContext = pantExContext;
            UserManager = userManager;
            SignInManager = signInManager;
            Logger = logger;
        }

        [HttpGet("info")]
        [Authorize]
        public async Task<IActionResult> Info()
        {
            return Ok(await UserManager.GetUserAsync(HttpContext.User));
        }

        [HttpPost("register")]
        public async Task<IActionResult> Register([Required] [FromForm] string username, [Required] [FromForm] string password)
        {
            var result = await UserManager.CreateAsync(new IdentityUser
            {
                UserName = username
            }, password);

            if (result.Succeeded)
            {
                Logger.LogInformation($"Created user {username}");
                return Created("/api/auth/info", "");
            }
            else
            {
                Logger.LogError($"Failed to create user {username}");
                foreach (var error in result.Errors)
                {
                    Logger.LogError(error.Description);
                }
                return StatusCode(500, "Failed to register");
            }
        }

        [HttpPost("login")]
        public async Task<IActionResult> Login([Required] [FromForm] string username, [Required] [FromForm] string password, [FromForm] string redirectTo)
        {
            var result = await SignInManager.PasswordSignInAsync(username, password, true, false);
            if (result.Succeeded)
            {
                Logger.LogInformation($"User {username} logged in");
                return Redirect(redirectTo ?? "/");
            }
            else
            {
                Logger.LogError($"Failed to log user {username} in");
                return StatusCode(500, "Failed to log in");
            }
        }

        [HttpPost("logout")]
        [Authorize]
        public async Task<IActionResult> Logout([FromForm] string redirectTo)
        {
            await SignInManager.SignOutAsync();
            return Redirect(redirectTo ?? "/");
        }
    }
}