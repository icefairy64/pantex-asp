using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Breezy.PantEX.ASP.Extensions;
using Breezy.PantEX.ASP.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Breezy.PantEX.ASP.Controllers
{
    [Route("/api/search/autocomplete")]
    [ApiController]
    public class AutocompleteController : ControllerBase
    {
        private readonly PantEXContext Context;

        public AutocompleteController(PantEXContext context)
        {
            Context = context;
        }
        
        [HttpGet]
        public async Task<ActionResult<List<AutocompleteSuggestion>>> GetSuggestions([FromQuery] string part)
        {
            if (part == null || part.Length < 3)
            {
                return new List<AutocompleteSuggestion>();
            }

            Func<string, string> tokenMapper = token => token;
            if (part[0] == '-')
            {
                part = part.Substring(1);
                tokenMapper = tokenMapper.Then(token => "-" + token);
            }
            var results = await Context.PostToTag
                .Where(tag => tag.Tag.ToLower().StartsWith(part.ToLower()))
                .GroupBy(tag => tag.Tag)
                .OrderByDescending(group => group.Count())
                .Take(20)
                .Select(group => new AutocompleteSuggestion
                {
                    Count = group.Count(),
                    Suggestion = tokenMapper(group.Key)
                })
                .ToListAsync();
            
            var staticResults = StaticSuggestions
                .Where(suggestion => suggestion.ToLower().StartsWith(part.ToLower()))
                .Take(20)
                .Select(suggestion => new AutocompleteSuggestion
                {
                    Suggestion = tokenMapper(suggestion)
                });

            return staticResults.Concat(results).Take(20).ToList();
        }
        
        private static List<string> StaticSuggestions;

        static AutocompleteController()
        {
            StaticSuggestions = new List<string>();
            StaticSuggestions.Add("pantex:favourite");
            StaticSuggestions.Add("order:random");
        }
        
        public class AutocompleteSuggestion
        {
            public string Suggestion { get; set; }
            public int? Count { get; set; }
        }
    }
}