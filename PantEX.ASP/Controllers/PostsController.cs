﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Microsoft.AspNetCore.WebUtilities;
using Breezy.PantEX.ASP.Utils;
using Microsoft.Net.Http.Headers;
using System.IO;
using System;
using System.ComponentModel.DataAnnotations;
using System.Text;
using Breezy.PantEX.ASP.Models;
using Microsoft.AspNetCore.Http;
using System.Text.Json;
using Microsoft.EntityFrameworkCore;
using Breezy.ContentUtil;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using System.Linq.Expressions;

namespace Breezy.PantEX.ASP.Controllers
{
    public class PostModel
    {
        public PostModel()
        {

        }

        public long? Id { get; set; }
        public string Description { get; set; }
        public List<string> Tags { get; set; }
        public List<AttachmentModel> Attachments { get; set; }
        public bool? Favourited { get; set; }
    }

    public class AttachmentModel
    {
        public AttachmentModel()
        {

        }

        public string Mimetype { get; set; }
        public int? Width { get; set; }
        public int? Height { get; set; }
        public short Role { get; set; }

        public string LocalPath { get; set; }
    }

    [Route("api/posts")]
    [ApiController]
    public class PostsController : ControllerBase
    {
        private readonly PantEXContext Context;

        private readonly FileStorage.FileStorage FileStorage;

        private readonly ImageUtil MetaExtractor;

        private readonly UserManager<IdentityUser> UserManager;

        public PostsController(PantEXContext context, FileStorage.FileStorage fileStorage, ImageUtil metaExtractor, UserManager<IdentityUser> userManager)
        {
            Context = context;
            FileStorage = fileStorage;
            MetaExtractor = metaExtractor;
            UserManager = userManager;
        }

        private static IQueryable<Post> ProcessQueryToken(IQueryable<Post> query, string token, IdentityUser user)
        {
            if (token.Length == 0)
            {
                return query;
            }
            
            var negated = false;
            if (token.StartsWith('-'))
            {
                negated = true;
                token = token.Substring(1);
            }

            if (token.StartsWith("pantex:"))
            {
                var args = token.Split(':');
                var criterion = args[1];
                return criterion switch
                {
                    "optimized" => query.WhereNegated(
                        post =>
                            post.Attachments.Any(a => ((AttachmentRole) a.Role).HasFlag(AttachmentRole.Preview)) &&
                            post.Attachments.Any(a => ((AttachmentRole) a.Role).HasFlag(AttachmentRole.Thumbnail)),
                        negated),
                    "favourite" => query.WhereNegated(
                        post => post.CollectionEntries.Any(e =>
                            e.User == user && e.CollectionName == CollectionsController.FavouritesCollectionName),
                        negated),
                    "collection" => query.WhereNegated(
                        post => 
                            post.CollectionEntries.Any(e =>
                                e.User == user && e.CollectionName.ToLower() == args[2].ToLower()),
                        negated),
                    _ => throw new Exception($"Unknown criterion: {criterion}")
                };
            }
            else
            {
                return query.WhereNegated(post => post.Tags.Any(x => x.Tag == token), negated);
            }
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<PostModel>>> GetPosts(int? offset, int? count, string query)
        {
            var tags = query?.Split(' ') ?? new string[0];

            var linq = Context.Posts
                .Include(post => post.Attachments)
                .Include(post => post.Tags)
                .Include(post => post.CollectionEntries)
                .OrderByDescending(post => post.Id) as IQueryable<Post>;

            if (tags.Contains("order:random"))
            {
                tags = tags.Where(tag => tag != "order:random").ToArray();
                var postCount = await linq.CountAsync();
                var rnd = new Random();
                linq = Enumerable.Range(0, count ?? 20)
                    .Select(i => linq.Skip(rnd.Next(postCount - 1)).Take(1))
                    .Aggregate((a, x) => a.Concat(x));
                offset = 0;
            }

            var userClaim = HttpContext.User;
            var user = userClaim == null ? null : await UserManager.GetUserAsync(userClaim);
            var userId = user?.Id;

            linq = tags
                .Select(x => x.ToLower())
                .Aggregate(linq, (current, tag) => ProcessQueryToken(current, tag, user));

            return linq
                .Skip(offset ?? 0)
                .Take(count ?? 20)
                .Select(post => new PostModel
                {
                    Id = post.Id,
                    Description = post.Description,
                    Tags = post.Tags.Select(tag => tag.Tag).ToList(),
                    Favourited = userClaim == null ? (bool?)null : post.CollectionEntries.Any(x => x.User.Id == userId && x.CollectionName == CollectionsController.FavouritesCollectionName),
                    Attachments = post.Attachments.Select(attachment => new AttachmentModel
                    {
                        Mimetype = attachment.Mimetype,
                        Width = attachment.VisualWidth,
                        Height = attachment.VisualHeight,
                        Role = attachment.Role,
                        LocalPath = attachment.LocalPath
                    }).ToList()
                })
                .ToList();
        }

        [HttpPost]
        public async Task<ActionResult> CreatePost([FromForm] string post, IFormFile file)
        {
            var postModel = JsonSerializer.Deserialize<PostModel>(post);
            //var postModel = post;

            var _post = new Post
            {
                Approved = false,
                Description = postModel.Description,
                Tags = postModel.Tags.Select(x => new PostToTag
                {
                    Tag = x
                }).ToList()
            };

            var tempFilePath = Path.GetTempFileName();
            await using (var stream = new FileStream(tempFilePath, FileMode.Create))
            {
                await file.CopyToAsync(stream);
            }

            ImageMetadata meta;
            IContentProcessor processor;
            
            await using (var stream = new FileStream(tempFilePath, FileMode.Open))
            {
                processor = await MetaExtractor.GetSupportingContentProcessorAsync(stream);
                if (processor == null)
                    throw new Exception("Unsupported format");
                if (processor.SupportsAsync)
                    meta = await processor.GetImageMetadataAsync(stream);
                else
                    meta = processor.GetImageMetadata(stream);
            }

            var extensions = processor.SuggestExtensions(meta.Mimetype);
            if (extensions.Length == 0)
            {
                throw new Exception("No matching extensions found for attachment");
            }

            var relPath = await FileStorage.StoreFile(tempFilePath, "." + extensions[0]);
            System.IO.File.Delete(tempFilePath);

            _post.Attachments = new List<Attachment>
            {
                new Attachment
                {
                    LocalPath = relPath,
                    Mimetype = meta.Mimetype,
                    VisualWidth = meta.Width,
                    VisualHeight = meta.Height,
                    Role = (short)AttachmentRole.Source
                }
            };

            await Context.Posts.AddAsync(_post);
            await Context.SaveChangesAsync();
            
            return Created("/posts/", "");
        }

        [HttpPatch]
        [Route("{postId}")]
        public async Task<ActionResult> UpdatePost([FromBody] PostModel postModel, [Required] long postId)
        {
            var post = await Context.FindAsync<Post>(postId);
            if (post == null)
            {
                return NotFound();
            }
            
            await Context.Entry(post)
                .Collection(p => p.Tags)
                .LoadAsync();

            if (postModel.Description != null)
            {
                post.Description = postModel.Description;
            }

            if (postModel.Tags != null)
            {
                var removedTags = post.Tags
                    .Where(t => !postModel.Tags.Exists(mt => mt == t.Tag));

                var addedTags = postModel.Tags
                    .Where(mt => post.Tags.All(t => t.Tag != mt));

                foreach (var postToTag in removedTags)
                {
                    post.Tags.Remove(postToTag);
                }

                foreach (var addedTag in addedTags)
                {
                    post.Tags.Add(new PostToTag
                    {
                        Tag = addedTag
                    });
                }
            }

            await Context.SaveChangesAsync();
            
            return Ok();
        }
    }
}
