using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using Breezy.PantEX.ASP.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Breezy.PantEX.ASP.Controllers
{
    public class PostToCollectionRequestModel
    {
        [JsonPropertyName("postId")]
        public int PostId { get; set; }
    }

    [Route("api/collections")]
    [ApiController]
    public class CollectionsController : ControllerBase
    {
        public const string FavouritesCollectionName = "Favourites";

        private readonly PantEXContext Context;
        private readonly UserManager<IdentityUser> UserManager;

        public CollectionsController(PantEXContext context, UserManager<IdentityUser> userManager)
        {
            Context = context;
            UserManager = userManager;
        }

        [HttpPost("{collectionName}/{postId}")]
        [Authorize]
        public async Task<ActionResult> AddPostToCollection([Required] string collectionName, [Required] long postId)
        {
            var user = await UserManager.GetUserAsync(HttpContext.User);
            var post = await Context.Posts.FindAsync(postId);
            if (post == null)
                return BadRequest($"Post with id {postId} not found");
            await Context.PostsToCollection
                .AddAsync(new PostToCollection
                {
                    User = user,
                    Post = post,
                    CollectionName = collectionName
                });
            await Context.SaveChangesAsync();
            return Ok();
        }

        [HttpDelete("{collectionName}/{postId}")]
        [Authorize]
        public async Task<ActionResult> RemovePostFromCollection([Required] string collectionName, [Required] long postId)
        {
            var user = await UserManager.GetUserAsync(HttpContext.User);
            var entry = await Context.PostsToCollection.FindAsync(postId, user.Id, collectionName);
            if (entry == null)
                return BadRequest($"Post with id {postId} not found in collection {collectionName}");
            Context.PostsToCollection.Remove(entry);
            await Context.SaveChangesAsync();
            return Ok();
        }

        
        [HttpGet]
        [Authorize]
        public async Task<ActionResult<List<CollectionModel>>> GetUserCollections()
        {
            var user = await UserManager.GetUserAsync(HttpContext.User);
            return await Context.PostsToCollection
                .Where(entry => entry.UserId == user.Id)
                .GroupBy(entry => entry.CollectionName)
                .Select(group => new CollectionModel
                {
                    Name = group.Key,
                    PostCount = group.Count()
                })
                .ToListAsync();
        }

        [HttpGet]
        [Authorize]
        [Route("post/{postId}")]
        public async Task<ActionResult<IList<string>>> GetPostCollections([Required] long postId)
        {
            var user = await UserManager.GetUserAsync(HttpContext.User);
            var post = await Context.Posts.FindAsync(postId);
            if (post == null)
                return NotFound();
            return await Context.PostsToCollection
                .Where(entry => entry.Post == post && entry.UserId == user.Id)
                .Select(entry => entry.CollectionName)
                .ToListAsync();
        }

        [HttpPatch]
        [Authorize]
        [Route("post/{postId}")]
        public async Task<ActionResult> UpdatePostCollections(
            [Required] long postId,
            [FromBody] IList<string> newCollectionNames)
        {
            var user = await UserManager.GetUserAsync(HttpContext.User);
            var post = await Context.Posts.FindAsync(postId);
            if (post == null)
                return NotFound();
            var currentCollectionNames = await Context.PostsToCollection
                .Where(entry => entry.Post == post && entry.UserId == user.Id)
                .Select(entry => entry.CollectionName)
                .ToListAsync();
            var deletedCollections = Context.PostsToCollection
                .Where(entry => entry.Post == post && entry.UserId == user.Id)
                .Where(entry => newCollectionNames.All(c => c != entry.CollectionName))
                .ToList();
            var addedCollections = newCollectionNames
                .Where(collection => currentCollectionNames.All(c => c != collection))
                .Select(collection => new PostToCollection
                {
                    Post = post,
                    User = user,
                    CollectionName = collection
                })
                .ToList();
            
            Context.PostsToCollection.RemoveRange(deletedCollections);
            await Context.PostsToCollection.AddRangeAsync(addedCollections);

            await Context.SaveChangesAsync();
            
            return Ok();
        }
    }

    public class CollectionModel
    {
        public string Name { get; set; }
        public int PostCount { get; set; }
    }
}