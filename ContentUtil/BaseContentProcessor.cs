using System.IO;
using System.Threading.Tasks;

namespace Breezy.ContentUtil
{
    public abstract class BaseContentProcessor : IContentProcessor
    {
        public bool SupportsFiles { get; } = true;
        public bool SupportsAsync { get; } = true;

        public abstract string[] SuggestExtensions(string mimetype);

        public virtual void Convert(Stream srcStream, Stream destStream, string mimetype, int? width, int? height)
        {
            ConvertAsync(srcStream, destStream, mimetype, width, width).Wait();
        }

        public virtual void Convert(FileInfo srcFile, FileInfo destFile, string mimetype, int? width, int? height)
        {
            using var srcStream = File.OpenRead(srcFile.FullName);
            using var destStream = File.OpenWrite(destFile.FullName);
            Convert(srcStream, destStream, mimetype, width, height);
        }

        public virtual Task ConvertAsync(Stream srcStream, Stream destStream, string mimetype, int? width, int? height)
        {
            return Task.Run(() => Convert(srcStream, destStream, mimetype, width, height));
        }

        public virtual async Task ConvertAsync(FileInfo srcFile, FileInfo destFile, string mimetype, int? width, int? height)
        {
            await using var srcStream = File.OpenRead(srcFile.FullName);
            await using var destStream = File.OpenWrite(destFile.FullName);
            await ConvertAsync(srcStream, destStream, mimetype, width, height);
        }

        public virtual ImageMetadata GetImageMetadata(Stream imageStream)
        {
            return GetImageMetadataAsync(imageStream).Result;
        }

        public virtual Task<ImageMetadata> GetImageMetadataAsync(Stream imageStream)
        {
            return Task.Run(() => GetImageMetadata(imageStream));
        }

        public virtual bool IsSupported(Stream contentStream)
        {
            return IsSupportedAsync(contentStream).Result;
        }

        public virtual Task<bool> IsSupportedAsync(Stream contentStream)
        {
            return Task.Run(() => IsSupported(contentStream));
        }
    }
}