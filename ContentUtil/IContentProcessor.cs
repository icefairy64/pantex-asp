using System.IO;
using System.Threading.Tasks;

namespace Breezy.ContentUtil
{
    public interface IContentProcessor
    {
        bool SupportsFiles { get; }
        bool SupportsAsync { get; }
        string[] SuggestExtensions(string mimetype);
        void Convert(Stream srcStream, Stream destStream, string mimetype, int? width, int? height);
        void Convert(FileInfo srcFile, FileInfo destFile, string mimetype, int? width, int? height);
        Task ConvertAsync(Stream srcStream, Stream destStream, string mimetype, int? width, int? height);
        Task ConvertAsync(FileInfo srcFile, FileInfo destFile, string mimetype, int? width, int? height);
        ImageMetadata GetImageMetadata(Stream imageStream);
        Task<ImageMetadata> GetImageMetadataAsync(Stream imageStream);
        bool IsSupported(Stream contentStream);
        Task<bool> IsSupportedAsync(Stream contentStream);
    }
}