namespace Breezy.ContentUtil
{
    public struct ImageMetadata
    {
        public int Width;
        public int Height;
        public string Mimetype;
    }
}