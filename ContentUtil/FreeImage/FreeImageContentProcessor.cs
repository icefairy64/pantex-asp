using System.IO;
using System.Threading.Tasks;
using FreeImageAPI;

namespace Breezy.ContentUtil.FreeImage
{
    public class FreeImageContentProcessor : BaseContentProcessor
    {
        public override bool IsSupported(Stream contentStream)
        {
            try
            {
                using var bitmap = FreeImageBitmap.FromStream(contentStream);
                return bitmap != null;
            }
            catch
            {
                return false;
            }
        }
        
        public override ImageMetadata GetImageMetadata(Stream imageStream)
        {
            using var bitmap = FreeImageBitmap.FromStream(imageStream);
            return new ImageMetadata
            {
                Width = bitmap.Width,
                Height = bitmap.Height,
                Mimetype = FreeImageAPI.FreeImage.GetFIFMimeType(bitmap.ImageFormat)
            };
        }

        public override string[] SuggestExtensions(string mimetype)
        {
            var fif = FreeImageAPI.FreeImage.GetFIFFromMime(mimetype);
            return FreeImageAPI.FreeImage.GetFIFExtensionList(fif).Split(',');
        }

        public override void Convert(Stream srcStream, Stream destStream, string mimetype, int? width, int? height)
        {
            using var bitmap = FreeImageBitmap.FromStream(srcStream);
            if (width.HasValue && height.HasValue)
            {
                bitmap.Rescale(width.Value, height.Value, FREE_IMAGE_FILTER.FILTER_LANCZOS3);
            }
            var fif = FreeImageAPI.FreeImage.GetFIFFromMime(mimetype);
            if (FreeImageAPI.FreeImage.FIFSupportsExportBPP(fif, 32) && bitmap.ColorDepth != 24)
            {
                bitmap.ConvertColorDepth(FREE_IMAGE_COLOR_DEPTH.FICD_32_BPP);
            }
            else
            {
                bitmap.ConvertColorDepth(FREE_IMAGE_COLOR_DEPTH.FICD_24_BPP);
            }
            if (!FreeImageAPI.FreeImage.FIFSupportsExportType(fif, bitmap.ImageType))
            {
                bitmap.ConvertType(FREE_IMAGE_TYPE.FIT_BITMAP, true);
            }
            bitmap.Save(destStream, fif);
        }
    }
}