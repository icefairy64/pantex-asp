using System;

namespace Breezy.ContentUtil.FfMpeg
{
    public static class Extensions
    {
        public static R Pipe<T, R>(this T? obj, Func<T, R> fn) where T : struct where R : class
        {
            return obj.HasValue ? fn(obj.Value) : null;
        }
        
        public static R Pipe<T, R>(this T obj, Func<T, R> fn)
        {
            return fn(obj);
        }
        
        public static R PipeSafe<T, R>(this T obj, Func<T, R> fn) where R : class
        {
            return obj == null ? null : fn(obj);
        }
        
        public static R? PipeSafe<T, R>(this T obj, Func<T, R?> fn) where R : struct
        {
            return obj == null ? (R?)null : fn(obj);
        }
    }
}