using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text.RegularExpressions;

namespace Breezy.ContentUtil.FfMpeg
{
    public class FfMpeg : IDisposable
    {
        private static readonly Regex CodecLineRegex = new Regex("\\s(.{6})\\s(\\S+)\\s+(.*)$");
        
        private Process FfMpegProcess;
        public List<CodecDescription> Codecs { get; private set; }

        public void Init()
        {
            FfMpegProcess = new Process
            {
                StartInfo =
                {
                    FileName = "ffmpeg"
                }
            };
            
            ProbeCodecs();
        }

        private void ProbeCodecs()
        {
            Codecs = new List<CodecDescription>();
            
            FfMpegProcess = new Process
            {
                StartInfo =
                {
                    FileName = "ffmpeg",
                    UseShellExecute = false,
                    RedirectStandardOutput = true,
                    Arguments = "-codecs -hide_banner"
                }
            };
            
            FfMpegProcess.Start();
            
            using (var standardOutput = FfMpegProcess.StandardOutput)
            {
                var delimiterReached = false;
                while (!standardOutput.EndOfStream)
                {
                    var line = standardOutput.ReadLine();
                    if (line != null && line.Contains("------"))
                    {
                        delimiterReached = true;
                    }
                    else if (delimiterReached)
                    {
                        Codecs.Add(LineToCodecDescription(line));
                    }
                }
            }
        }

        private static CodecDescription LineToCodecDescription(string line)
        {
            var match = CodecLineRegex.Match(line);
            var capStr = match.Groups[1].Value;
            var codecName = match.Groups[2].Value;
            var codecDesc = match.Groups[3].Value;
            return new CodecDescription
            {
                DecodingSupported = capStr[0] != '.',
                EncodingSupported = capStr[1] != '.',
                CodecType = CodecTypeFromLine(capStr[2]),
                IntraFrameOnly = capStr[3] != '.',
                LossyCompression = capStr[4] != '.',
                LosslessCompression = capStr[5] != '.',
                Name = codecName,
                Description = codecDesc
            };
        }

        private static CodecType CodecTypeFromLine(char codecTypeChar)
        {
            switch (codecTypeChar)
            {
                case 'V': return CodecType.Video;
                case 'A': return CodecType.Audio;
                case 'S': return CodecType.Subtitle;
                case 'D': return CodecType.Data;
                default: throw new Exception($"Unknown codec type char {codecTypeChar}");
            }
        }

        public void Dispose()
        {
            FfMpegProcess?.Dispose();
        }
    }
}