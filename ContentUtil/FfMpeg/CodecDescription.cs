namespace Breezy.ContentUtil.FfMpeg
{
    public struct CodecDescription
    {
        public bool DecodingSupported;
        public bool EncodingSupported;
        public CodecType CodecType;
        public bool IntraFrameOnly;
        public bool LossyCompression;
        public bool LosslessCompression;
        public string Name;
        public string Description;

        public override string ToString()
        {
            return $"{nameof(DecodingSupported)}: {DecodingSupported}, {nameof(EncodingSupported)}: {EncodingSupported}, {nameof(CodecType)}: {CodecType}, {nameof(IntraFrameOnly)}: {IntraFrameOnly}, {nameof(LossyCompression)}: {LossyCompression}, {nameof(LosslessCompression)}: {LosslessCompression}, {nameof(Name)}: {Name}, {nameof(Description)}: {Description}";
        }
    }

    public enum CodecType
    {
        Video,
        Audio,
        Subtitle,
        Data
    }
}