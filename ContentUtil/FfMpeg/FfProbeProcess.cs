using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace Breezy.ContentUtil.FfMpeg
{
    public class FfProbeProcess : FfMpegRunnable<FfProbeProcessResult>
    {
        private static readonly Regex KeyValueLineRegex = new Regex("^([^=]+)=([^=]+)$");

        private bool InFormatSection;
        private bool InStreamSection;
        private readonly FfProbeProcessResultBuilder ResultBuilder = new FfProbeProcessResultBuilder();

        private FfProbeProcess(Process process, Stream inputStream)
        {
            Process = process;
            InputStream = inputStream;
        }

        protected override void HandleProcessOutput(object sender, DataReceivedEventArgs e)
        {
            switch (e.Data)
            {
                case "[FORMAT]":
                    InFormatSection = true;
                    break;
                case "[/FORMAT]":
                    InFormatSection = false;
                    MarkSuccess(ResultBuilder.Build());
                    break;
                case "[STREAM]":
                    InStreamSection = true;
                    break;
                case "[/STREAM]":
                    InStreamSection = false;
                    break;
                default:
                {
                    if (InFormatSection || InStreamSection)
                    {
                        var tuple = e.Data
                            .PipeSafe(KeyValueLineRegex.Match)
                            .PipeSafe(x => x.Success ? x : null)
                            .PipeSafe(x => x.Groups.Select(g => g.Value))
                            .PipeSafe(x => ((string, string)?)(x.ElementAt(1), x.ElementAt(2)));
                        if (tuple.HasValue)
                        {
                            var (key, value) = tuple.Value;
                            if (InFormatSection)
                                ResultBuilder.ProcessFormatKeyValuePair(key, value);
                            else if (InStreamSection)
                                ResultBuilder.ProcessStreamKeyValuePair(key, value);
                        }
                    }
                    break;
                }
            }
        }

        protected override void HandleProcessExit(object sender, EventArgs e)
        {
            if (CancellationToken?.IsCancellationRequested == true || Task.Task.IsCompleted)
                return;
            
            // FIXME: in some cases, process exit handler is called before any input is received.
            // To avoid problems, MarkSuccess that should be there was removed.
            
            if (Process.ExitCode != 0)
                MarkFailure($"Process exited with code {Process.ExitCode}");
        }
        
        public static FfProbeProcess Create(string filePath)
        {
            return new FfProbeProcess(new Process
            {
                StartInfo =
                {
                    FileName = "ffprobe",
                    Arguments = $"-hide_banner -show_format -show_streams \"{filePath}\"",
                    RedirectStandardOutput = true,
                    UseShellExecute = false
                }
            }, null);
        }
        
        public static FfProbeProcess Create(Stream fileStream)
        {
            return new FfProbeProcess(new Process
            {
                StartInfo =
                {
                    FileName = "ffprobe",
                    Arguments = "-hide_banner -show_format -show_streams pipe:0",
                    RedirectStandardOutput = true,
                    RedirectStandardInput = true,
                    UseShellExecute = false
                }
            }, fileStream);
        }
    }

    public class FfProbeProcessResult
    {
        public string FormatName { get; set; }
        public TimeSpan Duration { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
    }

    internal class FfProbeProcessResultBuilder
    {
        private readonly FfProbeProcessResult Instance = new FfProbeProcessResult();
        
        public void ProcessFormatKeyValuePair(string key, string value)
        {
            switch (key)
            {
                case "format_name":
                    Instance.FormatName = value;
                    break;
                case "duration":
                    if (double.TryParse(value, out var result))
                        Instance.Duration = TimeSpan.FromSeconds(result);
                    break;
            }
        }

        public void ProcessStreamKeyValuePair(string key, string value)
        {
            switch (key)
            {
                case "width":
                    if (int.TryParse(value, out var width))
                        Instance.Width = width;
                    break;
                case "height":
                    if (int.TryParse(value, out var height))
                        Instance.Height = height;
                    break;
            }
        }

        public FfProbeProcessResult Build()
        {
            return Instance;
        }
    }
}