using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace Breezy.ContentUtil.FfMpeg
{
    public class FfMpegContentProcessor : BaseContentProcessor
    {
        private static readonly Dictionary<string, string> MimeToExtension;

        static FfMpegContentProcessor()
        {
            MimeToExtension = new Dictionary<string, string>
            {
                ["video/webm"] = "webm", 
                ["video/mp4"] = "mp4",
                ["image/webp"] = "webp"
            };
        }

        public override string[] SuggestExtensions(string mimetype)
        {
            return MimeToExtension.ContainsKey(mimetype)
                ? new[] { MimeToExtension[mimetype] }
                : new string[0];
        }

        public override async Task ConvertAsync(FileInfo srcFile, FileInfo destFile, string mimetype, int? width, int? height)
        {
            using var proc = FfMpegProcess.Create(new FfMpegProcessArgs
            {
                InputFilename = srcFile.FullName,
                OutputFilename = destFile.FullName,
                Scale = width != null && height != null ? $"{width}x{height}" : null,
                Frames = mimetype.StartsWith("image") ? 1 : (int?) null,
                Format = MimeToExtension.GetValueOrDefault(mimetype)
            });
            await proc.Run();
        }

        public override async Task<ImageMetadata> GetImageMetadataAsync(Stream imageStream)
        {
            using var proc = FfProbeProcess.Create(imageStream);
            var result = await proc.Run();
            var metadata = new ImageMetadata();
            
            if (result.FormatName.Contains("webm"))
                metadata.Mimetype = "video/webm";
            else if (result.FormatName.Contains("mp4"))
                metadata.Mimetype = "video/mp4";
            else
                throw new Exception($"Unsupported format {result.FormatName}");

            metadata.Width = result.Width;
            metadata.Height = result.Height;

            return metadata;
        }

        public override async Task<bool> IsSupportedAsync(Stream contentStream)
        {
            // FIXME
            try
            {
                var metadata = await GetImageMetadataAsync(contentStream);
                return metadata.Mimetype != null;
            }
            catch
            {
                return false;
            }
        }
    }
}