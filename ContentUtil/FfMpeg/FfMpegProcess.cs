using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace Breezy.ContentUtil.FfMpeg
{
    public abstract class FfMpegRunnable<T> : IDisposable
    {
        protected CancellationToken? CancellationToken;
        protected Stream InputStream;
        protected TaskCompletionSource<T> Task;
        
        public Process Process { get; protected set; }

        public event FfMpegProcessSuccessEventHandler<T> Completed;
        public event FfMpegProcessFailureEventHandler Failed;

        protected abstract void HandleProcessOutput(object sender, DataReceivedEventArgs e);
        protected abstract void HandleProcessExit(object sender, EventArgs e);
        
        public virtual Task<T> Run(CancellationToken? token = null)
        {
            Task = new TaskCompletionSource<T>();
            CancellationToken = token;
            CancellationToken?.Register(HandleTokenCancellation);

            Process.EnableRaisingEvents = true;
            
            Process.Exited += HandleProcessExit;
            Process.OutputDataReceived += HandleProcessOutput;
            
            Process.Start();
            
            Process.BeginOutputReadLine();
            
            var copyTask = CancellationToken == null
                ? InputStream?.CopyToAsync(Process.StandardInput.BaseStream)
                : InputStream?.CopyToAsync(Process.StandardInput.BaseStream, CancellationToken.Value);
            copyTask?.ContinueWith(t => Process.StandardInput.Close());
            
            return Task.Task;
        }

        private void HandleTokenCancellation()
        {
            Task.TrySetCanceled();
            if (!Process.HasExited)
            {
                Process.Kill();
            }
        }

        protected void MarkSuccess(T result)
        {
            Task.SetResult(result);
            Completed?.Invoke(this, new FfMpegProcessSuccessEventArgs<T>(result));
        }

        protected void MarkFailure(string message)
        {
            Task.SetException(new Exception(message));
            Failed?.Invoke(this, new FfMpegProcessFailureEventArgs(message));
        }
        
        public void Dispose()
        {
            Process?.Dispose();
        }
    }
    
    public class FfMpegProcess : FfMpegRunnable<FfMpegProcessResult>
    {
        private readonly FfMpegProcessArgs Args;
        private static readonly Regex KeyValueLineRegex = new Regex("^([^=]+)=([^=]+)$");

        private FfMpegProcess(Process process, FfMpegProcessArgs args)
        {
            Args = args;
            Process = process;
            InputStream = args.InputStream;
        }

        protected override void HandleProcessOutput(object sender, DataReceivedEventArgs e)
        {
            var tuple = e.Data
                .PipeSafe(KeyValueLineRegex.Match)
                .PipeSafe(x => x.Success ? x : null)
                .PipeSafe(x => x.Groups.Cast<Group>().Select(g => g.Value))
                .PipeSafe(x => ((string, string)?)(x.ElementAt(1), x.ElementAt(2)));

            if (!tuple.HasValue) 
                return;
            
            var (key, value) = tuple.Value;

            if (key != "progress")
                return;
            
            if (value == "end")
                MarkSuccess(new FfMpegProcessResult());
        }

        protected override void HandleProcessExit(object sender, EventArgs e)
        {
            if (CancellationToken?.IsCancellationRequested == true || Task.Task.IsCompleted)
                return;
            
            if (Process.ExitCode == 0)
                MarkSuccess(new FfMpegProcessResult());
            else
                MarkFailure($"Process exited with code {Process.ExitCode}");
        }

        public static FfMpegProcess Create(FfMpegProcessArgs args)
        {
            return new FfMpegProcess(new Process
            {
                StartInfo =
                {
                    FileName = "ffmpeg",
                    Arguments = args.BuildArgLine(),
                    RedirectStandardOutput = true,
                    RedirectStandardInput = args.InputStream != null,
                    UseShellExecute = false
                }
            }, args);
        }
    }

    public class FfMpegProcessArgs
    {
        public string OutputFilename { get; set; }
        public string InputFilename { get; set; }
        
        public int? QScale { get; set; }
        
        public Stream InputStream { get; set; }

        public int? Frames { get; set; }
        
        public string Scale { get; set; }
        public string Format { get; set; }

        private string BuildPostInputArgLine()
        {
            var builder = new StringBuilder();
            Frames?.Pipe(v => builder.AppendFormat(" -frames:v {0:D}", v));
            return builder.ToString();
        }
        
        private string BuildOutputArgLine()
        {
            var builder = new StringBuilder();
            QScale?.Pipe(v => builder.AppendFormat(" -qscale:{0:D}", v));
            Scale?.Pipe(v => builder.AppendFormat(" -s {0}", v));
            Format?.Pipe(v => builder.AppendFormat(" -f {0}", v));
            return builder.ToString();
        }
        
        public string BuildArgLine()
        {
            return $"-i \"{InputFilename}\" {BuildPostInputArgLine()} -y -hide_banner -progress pipe:1 {BuildOutputArgLine()} \"{OutputFilename}\"";
        }
    }

    public class FfMpegProcessFailureEventArgs : EventArgs
    {
        public string Message { get; }

        internal FfMpegProcessFailureEventArgs(string message)
        {
            Message = message;
        }
    }

    public class FfMpegProcessResult
    {
        
    }
    
    public class FfMpegProcessSuccessEventArgs<T> : EventArgs
    {
        public T Result { get; }
        
        internal FfMpegProcessSuccessEventArgs(T result)
        {
            Result = result;
        }
    }

    public delegate void FfMpegProcessSuccessEventHandler<T>(object sender, FfMpegProcessSuccessEventArgs<T> args);
    public delegate void FfMpegProcessFailureEventHandler(object sender, FfMpegProcessFailureEventArgs args);
}