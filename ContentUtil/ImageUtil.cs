﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Breezy.ContentUtil.FfMpeg;
using Breezy.ContentUtil.FreeImage;
using FreeImageAPI;

namespace Breezy.ContentUtil
{
    public class ImageUtil
    {
        private readonly List<IContentProcessor> ContentProcessors;
        
        public ImageUtil()
        {
            ContentProcessors = new List<IContentProcessor>
            {
                new FreeImageContentProcessor(),
                new FfMpegContentProcessor()
            };
        }

        private static T RunInAutorewind<T>(Stream stream, Func<Stream, T> fn)
        {
            try
            {
                return fn(stream);
            }
            finally
            {
                stream.Seek(0, SeekOrigin.Begin);
            }
        }
        
        private static async Task<T> RunInAutorewindAsync<T>(Stream stream, Func<Stream, Task<T>> fn)
        {
            try
            {
                return await fn(stream);
            }
            finally
            {
                stream.Seek(0, SeekOrigin.Begin);
            }
        }

        public async Task<IContentProcessor> GetSupportingContentProcessorAsync(Stream fileStream)
        {
            foreach (var proc in ContentProcessors)
            {
                if (proc.SupportsAsync && await RunInAutorewindAsync(fileStream, proc.IsSupportedAsync))
                    return proc;
                if (!proc.SupportsAsync && await Task.Run(() => RunInAutorewind(fileStream, proc.IsSupported)))
                    return proc;
            }
            return null;
        }

        public string[] SuggestExtensions(string mimetype)
        {
            return ContentProcessors
                .Select(proc => proc.SuggestExtensions(mimetype))
                .Aggregate(new string[0], (a, x) => a.Concat(x).ToArray());
        }
    }
}
