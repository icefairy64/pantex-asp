using System;
using System.IO;
using System.Linq;

namespace Breezy.ContentUtil.Tests
{
    public static class Util
    {
        private static readonly Random Random = new Random();

        public static string GetTempFileName(string extension)
        {
            var name = Enumerable.Range(0, 8)
                .Select(n => (char) (Random.Next(26) + 0x61))
                .Aggregate("", (a, x) => a + x);
            return Path.GetTempPath() + Path.PathSeparator + name + extension;
        }
    }
}