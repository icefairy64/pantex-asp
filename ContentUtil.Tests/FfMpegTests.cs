using System;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Breezy.ContentUtil.FfMpeg;
using NUnit.Framework;

namespace Breezy.ContentUtil.Tests
{
    public class FfMpegTests
    {
        private FfMpeg.FfMpeg FfMpeg;
        
        [SetUp]
        public void Setup()
        {
            FfMpeg = new FfMpeg.FfMpeg();
            FfMpeg.Init();
        }

        [Test]
        public void TestFfMpegCodecs()
        {
            Assert.True(FfMpeg.Codecs.Any(c => c.Name == "webp"), "No webp FFmpeg codec detected");
        }

        [Test]
        public async Task TestFfMpegWebpConversion()
        {
            var tmpFileName = Util.GetTempFileName(".webp");
            using var ffmpeg = FfMpegProcess.Create(new FfMpegProcessArgs
            {
                InputFilename = "Resources/sample_image_1.jpg",
                OutputFilename = tmpFileName
            });

            try
            {
                await ffmpeg.Run();
                await using var file = File.OpenRead(tmpFileName);
                Assert.True(file.Length > 0, "File is empty");
            }
            finally
            {
                File.Delete(tmpFileName);
            }
        }
        
        [Test]
        public async Task TestFfMpegWebpStreamConversion()
        {
            using var ctSource = new CancellationTokenSource();
            var ct = ctSource.Token;

            var tmpFileName = Util.GetTempFileName(".webp");
            await using var imageStream = File.OpenRead("Resources/sample_image_1.jpg");
            using var ffmpeg = FfMpegProcess.Create(new FfMpegProcessArgs
            {
                InputFilename = "pipe:0",
                InputStream = imageStream,
                OutputFilename = tmpFileName
            });

            try
            {
                var result = await ffmpeg.Run(ct);
                await using var file = File.OpenRead(tmpFileName);
                Assert.True(file.Length > 0, "File is empty");
            }
            finally
            {
                File.Delete(tmpFileName);
                ctSource.Cancel();
            }
        }

        [Test]
        public async Task TestFfProbeFileMetadata()
        {
            using var ctSource = new CancellationTokenSource();
            var ct = ctSource.Token;
            using var ffprobe = FfProbeProcess.Create("Resources/sample_image_1.jpg");
            try
            {
                var metadata = await ffprobe.Run(ct);
                Assert.AreEqual(4000, metadata.Width, "Width mismatch");
                Assert.AreEqual(3000, metadata.Height, "Height mismatch");
            }
            finally
            {
                ctSource.Cancel();
            }
        }
        
        [Test]
        public async Task TestFfProbeStreamMetadata()
        {
            using var ctSource = new CancellationTokenSource();
            var ct = ctSource.Token;
            await using var imageStream = File.OpenRead("Resources/sample_image_1.jpg");
            using var ffprobe = FfProbeProcess.Create(imageStream);
            try
            {
                var metadata = await ffprobe.Run(ct);
                Assert.AreEqual(4000, metadata.Width, "Width mismatch");
                Assert.AreEqual(3000, metadata.Height, "Height mismatch");
            }
            finally
            {
                ctSource.Cancel();
            }
        }
    }
}