using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;

namespace Breezy.PantEX.ASP.FileStorage
{
    public class FileStorage
    {   
        private IOptionsMonitor<FileStorageOptions> Options;

        public FileStorage(IOptionsMonitor<FileStorageOptions> options)
        {
            Options = options;
        }

        private string GetFileHash(string srcPath)
        {
            byte[] fileHashBytes;
            byte[] buffer = new byte[4096];
            var hash = SHA256.Create();

            using (var stream = new FileStream(srcPath, FileMode.Open))
            {
                fileHashBytes = hash.ComputeHash(stream);
            }

            var builder = new StringBuilder();
            foreach (var b in fileHashBytes)
            {
                builder.Append(String.Format("{0:x2}", b));
            }
            
            return builder.ToString();
        }

        public async Task<string> StoreFile(string srcPath, string extension = null)
        {
            var hash = GetFileHash(srcPath);
            var pathRoot = Options.CurrentValue.Path;

            var targetDirPath = Path.Combine(pathRoot, hash.Substring(0, 2), hash.Substring(2, 2));
            var targetDir = new DirectoryInfo(targetDirPath);
            if (!targetDir.Exists)
            {
                targetDir = Directory.CreateDirectory(targetDirPath);
            }

            var targetFilePath = Path.Combine(targetDirPath, hash + (extension == null ? Path.GetExtension(srcPath) : extension));

            using (var srcStream = new FileStream(srcPath, FileMode.Open))
            {
                using (var destStream = new FileStream(targetFilePath, FileMode.CreateNew))
                {
                    await srcStream.CopyToAsync(destStream);
                }
            }

            return Path.GetRelativePath(pathRoot, targetFilePath);
        }

        public async Task<string> StoreFileDirect(string srcPath, string destRelPath, bool createDirs = false)
        {
            using (var srcStream = new FileStream(srcPath, FileMode.Open))
            {
                return await StoreFileDirect(srcStream, destRelPath, createDirs);
            }
        }

        public async Task<string> StoreFileDirect(Stream srcStream, string destRelPath, bool createDirs = false)
        {
            var pathRoot = Options.CurrentValue.Path;
            var destPath = Path.Combine(pathRoot, destRelPath);

            if (createDirs)
            {
                Directory.CreateDirectory(Path.GetDirectoryName(destPath));
            }

            using (var destStream = new FileStream(destPath, FileMode.CreateNew))
            {
                await srcStream.CopyToAsync(destStream);
            }

            return destRelPath;
        }

        public string GetFilePath(string relPath)
        {
            var pathRoot = Options.CurrentValue.Path;
            var filePath = Path.Combine(pathRoot, relPath);
            if (File.Exists(filePath))
            {
                return filePath;
            }
            else
            {
                throw new FileNotFoundException($"Could not find file for relative path {relPath}");
            }
        }

        public FileStream GetFileStream(string relPath)
        {
            var pathRoot = Options.CurrentValue.Path;
            var filePath = Path.Combine(pathRoot, relPath);
            if (File.Exists(filePath))
            {
                return new FileStream(filePath, FileMode.Open);
            }
            else
            {
                throw new FileNotFoundException($"Could not find file for relative path {relPath}");
            }
        }
    }
}